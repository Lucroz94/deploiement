Echo Off

set iestartpage="http://intra.portail.domaine.fr/"
set wsus="http://wsus.domaine.fr"
set ntp="ntp.cs.domaine.fr"

::------------------------------------
ECHO.
ECHO Explorateur de fichier
::------------------------------------
:: Ajouter des d�tails sur les fichiers dans l'explorateur de fichiers (exe, dll, ocx)
REG ADD "HKLM\Software\Classes\dllfile" /v "PreviewDetails" /t REG_SZ /d "prop:System.FileVersion;System.Size;System.Kind;prop:System.Copyright;System.FileAttributes;System.SharingStatus;System.DateCreated;System.DateModified" /f
REG ADD "HKLM\Software\Classes\exefile" /v "PreviewDetails" /t REG_SZ /d "prop:System.FileVersion;System.Size;System.Kind;prop:System.Copyright;System.FileAttributes;System.SharingStatus;System.DateCreated;System.DateModified" /f
REG ADD "HKLM\Software\Classes\ocxfile" /v "PreviewDetails" /t REG_SZ /d "prop:System.FileVersion;System.Size;System.Kind;prop:System.Copyright;System.FileAttributes;System.SharingStatus;System.DateCreated;System.DateModified" /f

::Afficher le volet de visualisation
REG ADD "HKCU\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Modules\GlobalSettings\DetailsContainer" /v "DetailsContainer" /t REG_BINARY /d "0200000001000000" /f
REG ADD "HKCU\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Modules\GlobalSettings\Sizer" /v "DetailsContainerSizer" /t REG_BINARY /d "150100000100000000000000c0030000" /f

::Afficher le volet de d�tails
REG ADD "HKCU\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Modules\GlobalSettings\DetailsContainer" /v "DetailsContainer" /t REG_BINARY /d "0100000002000000" /f
REG ADD "HKCU\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Modules\GlobalSettings\DetailsContainer" /v "DetailsContainerSizer" /t REG_BINARY /d "150100000100000000000000c0030000" /f

:: Afficher la lettre du lecteur avant le label
REG ADD "HKCU\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer" /v "ShowDriveLettersFirst" /t REG_DWORD /d "4" /f

:: Ajout les icones dans l'active bar de l'explorateur de fichiers
REG ADD "HKCU\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Ribbon" /v "MinimizedStateTabletModeOff" /t REG_DWORD /d "1" /f
REG ADD "HKCU\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Ribbon" /v "QatItems" /t REG_BINARY /d "3c7369713a637573746f6d554920786d6c6e733a7369713d22687474703a2f2f736368656d61732e6d6963726f736f66742e636f6d2f77696e646f77732f323030392f726962626f6e2f716174223e3c7369713a726962626f6e206d696e696d697a65643d2274727565223e3c7369713a71617420706f736974696f6e3d2230223e3c7369713a736861726564436f6e74726f6c733e3c7369713a636f6e74726f6c206964513d227369713a3136313238222076697369626c653d2266616c73652220617267756d656e743d223022202f3e3c7369713a636f6e74726f6c206964513d227369713a3136313239222076697369626c653d2266616c73652220617267756d656e743d223022202f3e3c7369713a636f6e74726f6c206964513d227369713a3132333532222076697369626c653d2266616c73652220617267756d656e743d223022202f3e3c7369713a636f6e74726f6c206964513d227369713a3132333834222076697369626c653d22747275652220617267756d656e743d223022202f3e3c7369713a636f6e74726f6c206964513d227369713a3132333336222076697369626c653d22747275652220617267756d656e743d223022202f3e3c7369713a636f6e74726f6c206964513d227369713a3132333537222076697369626c653d2266616c73652220617267756d656e743d223022202f3e3c7369713a636f6e74726f6c206964513d227369713a3132323931222076697369626c653d22747275652220617267756d656e743d223022202f3e3c7369713a636f6e74726f6c206964513d227369713a3132333033222076697369626c653d22747275652220617267756d656e743d223022202f3e3c7369713a636f6e74726f6c206964513d227369713a3136343831222076697369626c653d22747275652220617267756d656e743d223022202f3e3c7369713a636f6e74726f6c206964513d227369713a3136353736222076697369626c653d22747275652220617267756d656e743d223022202f3e3c2f7369713a736861726564436f6e74726f6c733e3c2f7369713a7161743e3c2f7369713a726962626f6e3e3c2f7369713a637573746f6d55493e" /f

::------------------------------------
ECHO Divers
::------------------------------------
:: Supprimer la r�initialisation des applications par d�faut sur Windows 10
:: -------------------
:: Microsoft 3DBuilder
:: Type de fichiers: .stl, .3mf, .obj, .wrl, .ply, .fbx, .3ds, .dae, .dxf, .bmp, .jpg, .png, .tga
::-------------------
REG ADD "HKCU\SOFTWARE\Classes\AppXvhc4p7vz4b485xfp46hhk3fq3grkdgjg" /v "NoOpenWith" /t REG_SZ /d "" /f
REG ADD "HKCU\SOFTWARE\Classes\AppXvhc4p7vz4b485xfp46hhk3fq3grkdgjg" /v "NoStaticDefaultVerb" /t REG_SZ /d "" /f
::-------------------
:: Microsoft Edge
:: Type de fichiers�: .htm, .html
::-------------------
REG ADD "HKCU\SOFTWARE\Classes\AppX4hxtad77fbk3jkkeerkrm0ze94wjf3s9" /v "NoOpenWith" /t REG_SZ /d "" /f
REG ADD "HKCU\SOFTWARE\Classes\AppX4hxtad77fbk3jkkeerkrm0ze94wjf3s9" /v "NoStaticDefaultVerb" /t REG_SZ /d "" /f
:: Type de fichiers�: .pdf
REG ADD "HKCU\SOFTWARE\Classes\AppXd4nrz8ff68srnhf9t5a8sbjyar1cr723" /v "NoOpenWith" /t REG_SZ /d "" /f
REG ADD "HKCU\SOFTWARE\Classes\AppXd4nrz8ff68srnhf9t5a8sbjyar1cr723" /v "NoStaticDefaultVerb" /t REG_SZ /d "" /f
:: Type de fichiers�: .svg
REG ADD "HKCU\SOFTWARE\Classes\AppXde74bfzw9j31bzhcvsrxsyjnhhbq66cs" /v "NoOpenWith" /t REG_SZ /d "" /f
REG ADD "HKCU\SOFTWARE\Classes\AppXde74bfzw9j31bzhcvsrxsyjnhhbq66cs" /v "NoStaticDefaultVerb" /t REG_SZ /d "" /f
:: Type de fichiers�: .xml
REG ADD "HKCU\SOFTWARE\Classes\AppXcc58vyzkbjbs4ky0mxrmxf8278rk9b3t" /v "NoOpenWith" /t REG_SZ /d "" /f
REG ADD "HKCU\SOFTWARE\Classes\AppXcc58vyzkbjbs4ky0mxrmxf8278rk9b3t" /v "NoStaticDefaultVerb" /t REG_SZ /d "" /f
::-------------------
:: Microsoft Photos
:: Type de fichiers�: .3g2,.3gp, .3gp2, .3gpp, .asf, .avi, .m2t, .m2ts, .m4v, .mkv, .mov, .mp4, mp4v, .mts, .tif, .tiff, .wmv
::-------------------
REG ADD "HKCU\SOFTWARE\Classes\AppXk0g4vb8gvt7b93tg50ybcy892pge6jmt" /v "NoOpenWith" /t REG_SZ /d "" /f
REG ADD "HKCU\SOFTWARE\Classes\AppXk0g4vb8gvt7b93tg50ybcy892pge6jmt" /v "NoStaticDefaultVerb" /t REG_SZ /d "" /f
:: Type de fichiers�: La plupart des types de fichiers images
REG ADD "HKCU\SOFTWARE\Classes\AppX43hnxtbyyps62jhe9sqpdzxn1790zetc" /v "NoOpenWith" /t REG_SZ /d "" /f
REG ADD "HKCU\SOFTWARE\Classes\AppX43hnxtbyyps62jhe9sqpdzxn1790zetc" /v "NoStaticDefaultVerb" /t REG_SZ /d "" /f
:: Type de fichiers: .raw, .rwl, .rw2 et autres
REG ADD "HKCU\SOFTWARE\Classes\AppX9rkaq77s0jzh1tyccadx9ghba15r6t3h" /v "NoOpenWith" /t REG_SZ /d "" /f
REG ADD "HKCU\SOFTWARE\Classes\AppX9rkaq77s0jzh1tyccadx9ghba15r6t3h" /v "NoStaticDefaultVerb" /t REG_SZ /d "" /f
::-------------------
:: Zune Music
:: Type de fichiers�: .aac, .adt, .adts ,.amr, .flac, .m3u, .m4a, .m4r, .mp3, .mpa .wav, .wma, .wpl, .zpl
::-------------------
REG ADD "HKCU\SOFTWARE\Classes\AppXqj98qxeaynz6dv4459ayz6bnqxbyaqcs" /v "NoOpenWith" /t REG_SZ /d "" /f
REG ADD "HKCU\SOFTWARE\Classes\AppXqj98qxeaynz6dv4459ayz6bnqxbyaqcs" /v "NoStaticDefaultVerb" /t REG_SZ /d "" /f
::-------------------
:: Zune Video
:: Type de fichiers�: .3g2,.3gp, .3gpp, .avi, .divx, .m2t, .m2ts, .m4v, .mkv, .mod; .mov, .mp4, mp4v, .mpe, .mpeg, .mpg, .mpv2, .mts, .tod, .ts, .tts, .wm, .wmv, .xvid
::-------------------
REG ADD "HKCU\SOFTWARE\Classes\AppX6eg8h5sxqq90pv53845wmnbewywdqq5h" /v "NoOpenWith" /t REG_SZ /d "" /f
REG ADD "HKCU\SOFTWARE\Classes\AppX6eg8h5sxqq90pv53845wmnbewywdqq5h" /v "NoStaticDefaultVerb" /t REG_SZ /d "" /f


::---------------------------------------------------------------
ECHO.
ECHO Menu contextuel
::---------------------------------------------------------------
:: Ajouter 'Copier dans un dossier' dans le menu contextuel
REG ADD "HKLM\SOFTWARE\Classes\AllFilesystemObjects\shellex\ContextMenuHandlers\CopyTo" /ve /t REG_SZ /d "{C2FBB630-2971-11d1-A18C-00C04FD75D13}" /f
:: Ajouter 'D�placer vers un dossier' dans le menu contextuel
REG ADD "HKLM\SOFTWARE\Classes\AllFilesystemObjects\shellex\ContextMenuHandlers\MoveTo" /ve /t REG_SZ /d "{C2FBB631-2971-11d1-A18C-00C04FD75D13}" /f
:: Ajouter 'Copiez le chemin d�acc�s' toujours visible dans le menu contextuel
REG ADD "HKCR\Allfilesystemobjects\shell\windows.copyaspath" /v "CanonicalName" /t REG_SZ /d "{707C7BC6-685A-4A4D-A275-3966A5A3EFAA}" /f
REG ADD "HKCR\Allfilesystemobjects\shell\windows.copyaspath" /v "CommandStateHandler" /t REG_SZ /d "{3B1599F9-E00A-4BBF-AD3E-B3F99FA87779}" /f
REG ADD "HKCR\Allfilesystemobjects\shell\windows.copyaspath" /v "CommandStateSync" /t REG_SZ /d "" /f
REG ADD "HKCR\Allfilesystemobjects\shell\windows.copyaspath" /v "Description" /t REG_SZ /d "@shell32.dll,-30336" /f
REG ADD "HKCR\Allfilesystemobjects\shell\windows.copyaspath" /v "Icon" /t REG_SZ /d "imageres.dll,-5302" /f
REG ADD "HKCR\Allfilesystemobjects\shell\windows.copyaspath" /v "InvokeCommandOnSelection" /t REG_DWORD /d "1" /f
REG ADD "HKCR\Allfilesystemobjects\shell\windows.copyaspath" /v "MUIVerb" /t REG_SZ /d "@shell32.dll,-30329" /f
REG ADD "HKCR\Allfilesystemobjects\shell\windows.copyaspath" /v "VerbHandler" /t REG_SZ /d "{f3d06e7c-1e45-4a26-847e-f9fcdee59be0}" /f
REG ADD "HKCR\Allfilesystemobjects\shell\windows.copyaspath" /v "VerbName" /t REG_SZ /d "copyaspath" /f
:: Ajouter 'Supprimer de fa�on d�finitive' au menu contextuel
REG ADD "HKCR\*\shell\Windows.PermanentDelete" /v "CommandStateSync" /t REG_SZ /d "" /f
REG ADD "HKCR\*\shell\Windows.PermanentDelete" /v "ExplorerCommandHandler" /t REG_SZ /d "{E9571AB2-AD92-4EC6-8924-4E5AD33790F5}" /f
REG ADD "HKCR\*\shell\Windows.PermanentDelete" /v "Icon" /t REG_SZ /d "shell32.dll,-240" /f
:: Ajouter 'Copier la liste des fichiers du dossier dans le presse-papier' au menu contextuel
REG ADD "HKCR\Directory\shell\copylist" /ve /t REG_SZ /d "Copier la liste des fichiers du dossier dans le presse-papier" /f
REG ADD "HKCR\Directory\shell\copylist\command" /ve /t REG_SZ /d "cmd /c dir \"%%1\" /b /a:-d /o:n | clip" /f
REG ADD "HKCR\Directory\shell\copylist" /v "Icon" /t REG_SZ /d "imageres.dll,-5303" /f
:: D�sactiver 'Activer Bitlocker' dans le menu contextuel
REG ADD "HKCR\Drive\shell\change-passphrase" /v "ProgrammaticAccessOnly" /t REG_SZ /d "" /f
REG ADD "HKCR\Drive\shell\change-pin" /v "ProgrammaticAccessOnly" /t REG_SZ /d "" /f
REG ADD "HKCR\Drive\shell\encrypt-bde" /v "ProgrammaticAccessOnly" /t REG_SZ /d "" /f
REG ADD "HKCR\Drive\shell\encrypt-bde-elev" /v "ProgrammaticAccessOnly" /t REG_SZ /d "" /f
REG ADD "HKCR\Drive\shell\manage-bde" /v "ProgrammaticAccessOnly" /t REG_SZ /d "" /f
REG ADD "HKCR\Drive\shell\resume-bde" /v "ProgrammaticAccessOnly" /t REG_SZ /d "" /f
REG ADD "HKCR\Drive\shell\resume-bde-elev" /v "ProgrammaticAccessOnly" /t REG_SZ /d "" /f
REG ADD "HKCR\Drive\shell\unlock-bde" /v "ProgrammaticAccessOnly" /t REG_SZ /d "" /f

::---------------------------------------------------------------
ECHO.
ECHO Bureau
::---------------------------------------------------------------
:: Afficher le menu 'Apparence' lorsque l'on fait un clic droit sur le bureau
REG ADD "HKCR\DesktopBackground\Shell\Apparence" /v "Icon" /t REG_SZ /d "themecpl.dll" /f
REG ADD "HKCR\DesktopBackground\Shell\Apparence" /v "MUIVerb" /t REG_SZ /d "Apparence" /f
REG ADD "HKCR\DesktopBackground\Shell\Apparence" /v "Position" /t REG_SZ /d "Bottom" /f
REG ADD "HKCR\DesktopBackground\Shell\Apparence" /v "SubCommands" /t REG_SZ /d "" /f
REG ADD "HKCR\DesktopBackground\Shell\Apparence\Shell\01DesktopBackground" /v "Icon" /t REG_SZ /d "imageres.dll,-110" /f
REG ADD "HKCR\DesktopBackground\Shell\Apparence\Shell\01DesktopBackground" /v "MUIVerb" /t REG_SZ /d "Arri�re-plan du bureau" /f
REG ADD "HKCR\DesktopBackground\Shell\Apparence\Shell\01DesktopBackground\Command" /ve /t REG_SZ /d "control.exe /NAME Microsoft.Personalization /PAGE pageWallpaper" /f
REG ADD "HKCR\DesktopBackground\Shell\Apparence\Shell\02Color" /v "Icon" /t REG_SZ /d "themecpl.dll" /f
REG ADD "HKCR\DesktopBackground\Shell\Apparence\Shell\02Color" /v "MUIVerb" /t REG_SZ /d "Couleur" /f
REG ADD "HKCR\DesktopBackground\Shell\Apparence\Shell\02Color\Command" /ve /t REG_SZ /d "control.exe /NAME Microsoft.Personalization /PAGE pageColorization" /f
REG ADD "HKCR\DesktopBackground\Shell\Apparence\Shell\03Sounds" /v "Icon" /t REG_SZ /d "mmsys.cpl" /f
REG ADD "HKCR\DesktopBackground\Shell\Apparence\Shell\03Sounds" /v "MUIVerb" /t REG_SZ /d "Sons" /f
REG ADD "HKCR\DesktopBackground\Shell\Apparence\Shell\03Sounds\Command" /ve /t REG_SZ /d "rundll32.exe shell32.dll,Control_RunDLL mmsys.cpl ,2" /f
REG ADD "HKCR\DesktopBackground\Shell\Apparence\Shell\04Screen Saver" /v "Icon" /t REG_SZ /d "PhotoScreensaver.scr" /f
REG ADD "HKCR\DesktopBackground\Shell\Apparence\Shell\04Screen Saver" /v "MUIVerb" /t REG_SZ /d "�cran de veille" /f
REG ADD "HKCR\DesktopBackground\Shell\Apparence\Shell\04Screen Saver\Command" /ve /t REG_SZ /d "rundll32.exe shell32.dll,Control_RunDLL desk.cpl,screensaver,@screensaver" /f
REG ADD "HKCR\DesktopBackground\Shell\Apparence\Shell\05DesktopIcons" /v "Icon" /t REG_SZ /d "desk.cpl" /f
REG ADD "HKCR\DesktopBackground\Shell\Apparence\Shell\05DesktopIcons" /v "MUIVerb" /t REG_SZ /d "Changer les ic�nes du bureau" /f
REG ADD "HKCR\DesktopBackground\Shell\Apparence\Shell\05DesktopIcons" /v "CommandFlags" /t REG_DWORD /d "32" /f
REG ADD "HKCR\DesktopBackground\Shell\Apparence\Shell\05DesktopIcons\Command" /ve /t REG_SZ /d "rundll32 shell32.dll,Control_RunDLL desk.cpl,,0" /f
REG ADD "HKCR\DesktopBackground\Shell\Apparence\Shell\06Cursors" /v "Icon" /t REG_SZ /d "main.cpl" /f
REG ADD "HKCR\DesktopBackground\Shell\Apparence\Shell\06Cursors" /v "MUIVerb" /t REG_SZ /d "Changer les pointeurs de souris" /f
REG ADD "HKCR\DesktopBackground\Shell\Apparence\Shell\06Cursors\Command" /ve /t REG_SZ /d "rundll32.exe shell32.dll,Control_RunDLL main.cpl,,1" /f

:: Afficher le menu 'Gestion �nergie' lorsque l'on fait un clic droit sur le bureau
REG ADD "HKCR\DesktopBackground\Shell\GestionEnergie" /v "Icon" /t REG_SZ /d "powercpl.dll" /f
REG ADD "HKCR\DesktopBackground\Shell\GestionEnergie" /v "MUIVerb" /t REG_SZ /d "Gestion de l'alimentation" /f
REG ADD "HKCR\DesktopBackground\Shell\GestionEnergie" /v "Position" /t REG_SZ /d "Bottom" /f
REG ADD "HKCR\DesktopBackground\Shell\GestionEnergie" /v "SubCommands" /t REG_SZ /d "" /f
REG ADD "HKCR\DesktopBackground\Shell\GestionEnergie\Shell\01PowerSaver" /v "Icon" /t REG_SZ /d "powercpl.dll" /f
REG ADD "HKCR\DesktopBackground\Shell\GestionEnergie\Shell\01PowerSaver" /v "MUIVerb" /t REG_SZ /d "�conomie d'�nergie" /f
REG ADD "HKCR\DesktopBackground\Shell\GestionEnergie\Shell\01PowerSaver\Command" /ve /t REG_SZ /d "powercfg.exe /S a1841308-3541-4fab-bc81-f71556f20b4a" /f
REG ADD "HKCR\DesktopBackground\Shell\GestionEnergie\Shell\02Balanced" /v "Icon" /t REG_SZ /d "powercpl.dll" /f
REG ADD "HKCR\DesktopBackground\Shell\GestionEnergie\Shell\02Balanced" /v "MUIVerb" /t REG_SZ /d "Utilisation normale" /f
REG ADD "HKCR\DesktopBackground\Shell\GestionEnergie\Shell\02Balanced\Command" /ve /t REG_SZ /d "powercfg.exe /S 381b4222-f694-41f0-9685-ff5bb260df2e" /f
REG ADD "HKCR\DesktopBackground\Shell\GestionEnergie\Shell\03HighPerformance" /v "Icon" /t REG_SZ /d "powercpl.dll" /f
REG ADD "HKCR\DesktopBackground\Shell\GestionEnergie\Shell\03HighPerformance" /v "MUIVerb" /t REG_SZ /d "Performances �lev�es" /f
REG ADD "HKCR\DesktopBackground\Shell\GestionEnergie\Shell\03HighPerformance\Command" /ve /t REG_SZ /d "powercfg.exe /S 8c5e7fda-e8bf-4a96-9a85-a6e23a8c635c" /f

:: Affichage Ic�nes sur le bureau (0=active 1=d�sactive)
REG ADD "HKCU\Software\Microsoft\Windows\CurrentVersion\Explorer\HideDesktopIcons\NewStartPanel" /v "{5399E694-6CE5-4D6C-8FCE-1D8870FDCBA0}" /t REG_DWORD /d "0" /f
REG ADD "HKCU\Software\Microsoft\Windows\CurrentVersion\Explorer\HideDesktopIcons\NewStartPanel" /v "{59031a47-3f72-44a7-89c5-5595fe6b30ee}" /t REG_DWORD /d "0" /f
REG ADD "HKCU\Software\Microsoft\Windows\CurrentVersion\Explorer\HideDesktopIcons\NewStartPanel" /v "{645FF040-5081-101B-9F08-00AA002F954E}" /t REG_DWORD /d "0" /f
REG ADD "HKCU\Software\Microsoft\Windows\CurrentVersion\Explorer\HideDesktopIcons\NewStartPanel" /v "{F02C1A0D-BE21-4350-88B0-7367FC96EF3C}" /t REG_DWORD /d "0" /f
REG ADD "HKCU\Software\Microsoft\Windows\CurrentVersion\Explorer\HideDesktopIcons\NewStartPanel" /v "{20D04FE0-3AEA-1069-A2D8-08002B30309D}" /t REG_DWORD /d "0" /f

:: Afficher l'ic�ne de recherche (loupe) dans la barre des t�ches � la place de cortana
REG ADD "HKCU\Software\Microsoft\Windows\CurrentVersion\Search" /v "SearchboxTaskbarMode" /t REG_DWORD /d "1" /f

:: Ajoute le Panneau de configuration au menu contextuel du bureau
REG ADD "HKCR\DesktopBackground\Shell\ControlPanel" /v "MUIVerb" /t REG_SZ /d "@shell32.dll,-4161" /f
REG ADD "HKCR\DesktopBackground\Shell\ControlPanel" /v "SubCommands" /t REG_SZ /d "" /f
REG ADD "HKCR\DesktopBackground\Shell\ControlPanel" /v "Icon" /t REG_SZ /d "imageres.dll,-27" /f
REG ADD "HKCR\DesktopBackground\Shell\ControlPanel" /v "Position" /t REG_SZ /d "Bottom" /f
REG ADD "HKCR\DesktopBackground\Shell\ControlPanel\Shell\1ControlPanelCmd" /ve /t REG_SZ /d "@shell32.dll,-31061" /f
REG ADD "HKCR\DesktopBackground\Shell\ControlPanel\Shell\1ControlPanelCmd" /v "Icon" /t REG_SZ /d "imageres.dll,-27" /f
REG ADD "HKCR\DesktopBackground\Shell\ControlPanel\Shell\1ControlPanelCmd\command" /ve /t REG_SZ /d "explorer.exe shell:::{26EE0668-AA-44D7-9371-BEB064C98683}" /f
REG ADD "HKCR\DesktopBackground\Shell\ControlPanel\Shell\2ControlPanelCmd" /ve /t REG_SZ /d "@shell32.dll,-31062" /f
REG ADD "HKCR\DesktopBackground\Shell\ControlPanel\Shell\2ControlPanelCmd" /v "Icon" /t REG_SZ /d "imageres.dll,-27" /f
REG ADD "HKCR\DesktopBackground\Shell\ControlPanel\Shell\2ControlPanelCmd\command" /ve /t REG_SZ /d "explorer.exe shell:::{21EC2020-3AEA-1069-A2DD-082B30309D}" /f
REG ADD "HKCR\DesktopBackground\Shell\ControlPanel\Shell\3ControlPanelCmd" /ve /t REG_SZ /d "@shell32.dll,-32537" /f
REG ADD "HKCR\DesktopBackground\Shell\ControlPanel\Shell\3ControlPanelCmd" /v "Icon" /t REG_SZ /d "imageres.dll,-27" /f
REG ADD "HKCR\DesktopBackground\Shell\ControlPanel\Shell\3ControlPanelCmd" /v "CommandFlags" /t REG_DWORD /d "32" /f
REG ADD "HKCR\DesktopBackground\Shell\ControlPanel\Shell\3ControlPanelCmd\command" /ve /t REG_SZ /d "explorer.exe shell:::{ED7BA470-8E54-465E-825C-99712043E01C}" /f


::---------------------------------------------------------------
ECHO.
ECHO Panneau de configuration
::---------------------------------------------------------------
:: Petites icones dans le panneau de configuration
REG ADD "HKCU\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\ControlPanel" /v "StartupPage" /t REG_DWORD /d "1" /f
REG ADD "HKCU\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\ControlPanel" /v "AllItemsIconView" /t REG_DWORD /d "1" /f

:: Ajouter 'Windows Update' dans le panneau de configuration
REG ADD "HKLM\SOFTWARE\Classes\CLSID\{36eef7db-88ad-4e81-ad49-0e313f0c35f8}" /ve /t REG_SZ /d "@%%SystemRoot%%\system32\shell32.dll,-22068" /f
REG ADD "HKLM\SOFTWARE\Classes\CLSID\{36eef7db-88ad-4e81-ad49-0e313f0c35f8}" /v "System.Software.TasksFileUrl" /t REG_SZ /d "Internal" /f
REG ADD "HKLM\SOFTWARE\Classes\CLSID\{36eef7db-88ad-4e81-ad49-0e313f0c35f8}" /v "System.ApplicationName" /t REG_SZ /d "Microsoft.WindowsUpdate" /f
REG ADD "HKLM\SOFTWARE\Classes\CLSID\{36eef7db-88ad-4e81-ad49-0e313f0c35f8}" /v "System.ControlPanel.Category" /t REG_SZ /d "5" /f
REG ADD "HKLM\SOFTWARE\Classes\CLSID\{36eef7db-88ad-4e81-ad49-0e313f0c35f8}" /v "InfoTip" /t REG_SZ /d "@%%SystemRoot%%\system32\shell32.dll,-22580" /f
REG ADD "HKLM\SOFTWARE\Classes\CLSID\{36eef7db-88ad-4e81-ad49-0e313f0c35f8}\DefaultIcon" /ve /t REG_EXPAND_SZ /d "shell32.dll,-47" /f
REG ADD "HKLM\SOFTWARE\Classes\CLSID\{36eef7db-88ad-4e81-ad49-0e313f0c35f8}\Shell\Open\Command" /ve /t REG_EXPAND_SZ /d "control.exe /name Microsoft.WindowsUpdate" /f
REG ADD "HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\ControlPanel\NameSpace\{36eef7db-88ad-4e81-ad49-0e313f0c35f8}" /ve /t REG_SZ /d "Windows Update" /f

:: Ajouter le module "Editeur du registre" au Panneau de configuration
REG ADD "HKLM\Software\Classes\CLSID\{77708248-f839-436b-8919-527c410f48b9}" /ve /t REG_SZ /d "�diteur de Registre" /f
REG ADD "HKLM\Software\Classes\CLSID\{77708248-f839-436b-8919-527c410f48b9}" /v "InfoTip" /t REG_SZ /d "D�marre l'�diteur de Registre" /f
REG ADD "HKLM\Software\Classes\CLSID\{77708248-f839-436b-8919-527c410f48b9}" /v "System.ControlPanel.Category" /t REG_SZ /d "5" /f
REG ADD "HKLM\Software\Classes\CLSID\{77708248-f839-436b-8919-527c410f48b9}\DefaultIcon" /ve /t REG_SZ /d "%%SystemRoot%%\regedit.exe" /f
REG ADD "HKLM\Software\Classes\CLSID\{77708248-f839-436b-8919-527c410f48b9}\Shell\Open\Command" /ve /t REG_EXPAND_SZ /d "regedit.exe" /f
REG ADD "HKLM\Software\Microsoft\Windows\CurrentVersion\Explorer\ControlPanel\NameSpace\{77708248-f839-436b-8919-527c410f48b9}" /ve /t REG_SZ /d "Ajoute �diteur de Registre au Panneau de configuration" /f

:: Ajouter le module "Editeur de strat�gie de groupe locale" au Panneau de configuration
REG ADD "HKLM\Software\Classes\CLSID\{9cd0827e-0ad1-4c27-93d0-29f4c4ecd3b2}" /ve /t REG_SZ /d "�diteur de strat�gie de groupe" /f
REG ADD "HKLM\Software\Classes\CLSID\{9cd0827e-0ad1-4c27-93d0-29f4c4ecd3b2}" /v "InfoTip" /t REG_SZ /d "D�marre l'�diteur de strat�gie de groupe locale" /f
REG ADD "HKLM\Software\Classes\CLSID\{9cd0827e-0ad1-4c27-93d0-29f4c4ecd3b2}" /v "System.ControlPanel.Category" /t REG_SZ /d "5" /f
REG ADD "HKLM\Software\Classes\CLSID\{9cd0827e-0ad1-4c27-93d0-29f4c4ecd3b2}\DefaultIcon" /ve /t REG_SZ /d "%%SystemRoot%%\System32\gpedit.dll" /f
REG ADD "HKLM\Software\Classes\CLSID\{9cd0827e-0ad1-4c27-93d0-29f4c4ecd3b2}\Shell\Open\Command" /ve /t REG_EXPAND_SZ /d "mmc.exe gpedit.msc" /f
REG ADD "HKLM\Software\Microsoft\Windows\CurrentVersion\Explorer\ControlPanel\NameSpace\{9cd0827e-0ad1-4c27-93d0-29f4c4ecd3b2}" /ve /t REG_SZ /d "Ajoute l'�diteur de strat�gie de groupe locale au Panneau de configuration" /f

:: Ajouter le module "Gestion avanc�e des comptes d'utilisateurs" au Panneau de configuration
REG ADD "HKLM\Software\Classes\CLSID\{98641F47-8C25-4936-BEE4-C2CE1298969D}" /ve /t REG_SZ /d "Comptes d'utilisateurs [Avanc�]" /f
REG ADD "HKLM\Software\Classes\CLSID\{98641F47-8C25-4936-BEE4-C2CE1298969D}" /v "InfoTip" /t REG_SZ /d "Gestion avanc�e des comptes d'utilisateurs, style Windows 2000." /f
REG ADD "HKLM\Software\Classes\CLSID\{98641F47-8C25-4936-BEE4-C2CE1298969D}" /v "System.ControlPanel.Category" /t REG_SZ /d "9" /f
REG ADD "HKLM\Software\Classes\CLSID\{98641F47-8C25-4936-BEE4-C2CE1298969D}\DefaultIcon" /ve /t REG_SZ /d "%%SystemRoot%%\System32\EhStorAuthn.exe,0" /f
REG ADD "HKLM\Software\Classes\CLSID\{98641F47-8C25-4936-BEE4-C2CE1298969D}\Shell\Open\command" /ve /t REG_SZ /d "Control Userpasswords2" /f
REG ADD "HKLM\Software\Microsoft\Windows\CurrentVersion\Explorer\ControlPanel\NameSpace\{98641F47-8C25-4936-BEE4-C2CE1298969D}" /ve /t REG_SZ /d "Ajoute le module Comptes d'utilisateurs avanc� au Panneau de configuration" /f

:: Ajout des "Services" pour Windows dans le Panneau de configuration
REG ADD "HKLM\Software\Classes\CLSID\{D14ED2E1-C75B-443c-BD7C-FC03B2F08C01}" /ve /t REG_SZ /d "Services" /f
REG ADD "HKLM\Software\Classes\CLSID\{D14ED2E1-C75B-443c-BD7C-FC03B2F08C01}" /v "InfoTip" /t REG_SZ /d "D�marre, arr�te et configure les services Windows." /f
REG ADD "HKLM\Software\Classes\CLSID\{D14ED2E1-C75B-443c-BD7C-FC03B2F08C01}" /v "System.ControlPanel.Category" /t REG_SZ /d "5" /f
REG ADD "HKLM\Software\Classes\CLSID\{D14ED2E1-C75B-443c-BD7C-FC03B2F08C01}\DefaultIcon" /ve /t REG_SZ /d "%%SystemRoot%%\system32\filemgmt.dll,0" /f
REG ADD "HKLM\Software\Classes\CLSID\{D14ED2E1-C75B-443c-BD7C-FC03B2F08C01}\Shell\Open\Command" /ve /t REG_SZ /d "mmc.exe services.msc" /f
REG ADD "HKLM\Software\Classes\CLSID\{D14ED2E1-C75B-443c-BD7C-FC03B2F08C01}\ShellFolder" /v "Attributes" /t REG_DWORD /d "0" /f
REG ADD "HKLM\Software\Microsoft\Windows\CurrentVersion\Explorer\ControlPanel\NameSpace\{D14ED2E1-C75B-443c-BD7C-FC03B2F08C01}" /ve /t REG_SZ /d "Ajoute le module Services au panneau de configuration" /f

:: Ajout du "Gestionnaire des t�ches" dans le Panneau de configuration
REG ADD "HKLM\Software\Classes\CLSID\{D14ED2E1-C75B-443c-BD7C-FC03B2F08C06}" /ve /t REG_SZ /d "Gestionnaire des t�ches" /f
REG ADD "HKLM\Software\Classes\CLSID\{D14ED2E1-C75B-443c-BD7C-FC03B2F08C06}" /v "InfoTip" /t REG_SZ /d "Le Gestionnaire des t�ches Windows affiche l'utilisation du CPU, les processus en cours, le taux de m�moire disponible..." /f
REG ADD "HKLM\Software\Classes\CLSID\{D14ED2E1-C75B-443c-BD7C-FC03B2F08C06}" /v "System.ControlPanel.Category" /t REG_SZ /d "5" /f
REG ADD "HKLM\Software\Classes\CLSID\{D14ED2E1-C75B-443c-BD7C-FC03B2F08C06}\DefaultIcon" /ve /t REG_SZ /d "%%SystemRoot%%\system32\taskmgr.exe,0" /f
REG ADD "HKLM\Software\Classes\CLSID\{D14ED2E1-C75B-443c-BD7C-FC03B2F08C06}\Shell\Open\Command" /ve /t REG_SZ /d "Taskmgr.exe" /f
REG ADD "HKLM\Software\Classes\CLSID\{D14ED2E1-C75B-443c-BD7C-FC03B2F08C06}\ShellFolder" /v "Attributes" /t REG_DWORD /d "0" /f
REG ADD "HKLM\Software\Microsoft\Windows\CurrentVersion\Explorer\ControlPanel\NameSpace\{D14ED2E1-C75B-443c-BD7C-FC03B2F08C06}" /ve /t REG_SZ /d "Ajoute le module Gestionnaire des t�ches au panneau de configuration" /f

:: Ajout de "Msconfig" dans le Panneau de configuration
REG ADD "HKLM\Software\Classes\CLSID\{D14ED2E1-C75B-443c-BD7C-FC03B2F08C15}" /ve /t REG_SZ /d "Msconfig" /f
REG ADD "HKLM\Software\Classes\CLSID\{D14ED2E1-C75B-443c-BD7C-FC03B2F08C15}" /v "InfoTip" /t REG_SZ /d "Msconfig est un utilitaire de Windows qui permet d'extraire des fichiers, d'activer ou d�sactiver des services et des �l�ments de d�marrage." /f
REG ADD "HKLM\Software\Classes\CLSID\{D14ED2E1-C75B-443c-BD7C-FC03B2F08C15}" /v "System.ControlPanel.Category" /t REG_SZ /d "5" /f
REG ADD "HKLM\Software\Classes\CLSID\{D14ED2E1-C75B-443c-BD7C-FC03B2F08C15}\DefaultIcon" /ve /t REG_SZ /d "%%SystemRoot%%\system32\msconfig.exe,0" /f
REG ADD "HKLM\Software\Classes\CLSID\{D14ED2E1-C75B-443c-BD7C-FC03B2F08C15}\Shell\Open\Command" /ve /t REG_SZ /d "msconfig.exe" /f
REG ADD "HKLM\Software\Classes\CLSID\{D14ED2E1-C75B-443c-BD7C-FC03B2F08C15}\ShellFolder" /v "Attributes" /t REG_DWORD /d "0" /f
REG ADD "HKLM\Software\Microsoft\Windows\CurrentVersion\Explorer\ControlPanel\NameSpace\{D14ED2E1-C75B-443c-BD7C-FC03B2F08C15}" /ve /t REG_SZ /d "Ajoute le module Msconfig au panneau de configuration" /f

::---------------------------------------------------------------
ECHO.
ECHO Effets visuels
::---------------------------------------------------------------
:: D�sactive l'animation de fen�tre lors du redimensionnement
REG ADD "HKCU\Control Panel\Desktop\WindowMetrics" /v "MinAnimate" /t REG_SZ /d "0" /f
:: D�sactive les animations
REG ADD "HKCU\SOFTWARE\Policies\Microsoft\Windows\DWM" /v "DisallowAnimations" /t REG_DWORD /d "1" /f
:: Autres
REG ADD "HKCU\Control Panel\Desktop" /v "AutoEndTasks" /t REG_SZ /d "1" /f
REG ADD "HKCU\Control Panel\Desktop" /v "DragFullWindows" /t REG_SZ /d "0" /f
REG ADD "HKCU\Control Panel\Desktop" /v "HungAppTimeout" /t REG_SZ /d "2000" /f
REG ADD "HKCU\Control Panel\Desktop" /v "LowLevelHooksTimeout" /t REG_SZ /d "2000" /f
REG ADD "HKCU\Control Panel\Desktop" /v "MenuShowDelay" /t REG_SZ /d "200" /f
REG ADD "HKCU\Control Panel\Desktop" /v "UserPreferencesMask" /t REG_BINARY /d "9012078010000000" /f
REG ADD "HKCU\Control Panel\Desktop" /v "WaitToKillAppTimeout" /t REG_SZ /d "5000" /f
REG ADD "HKCU\Control Panel\Desktop" /v "WaitToKillServiceTimeout" /t REG_SZ /d "5000" /f


::D�sactiver le 1er �cran de v�rouillage
::gpedit.msc / Configuration de l�ordinateur / Mod�les d�administration / Panneau de configurtation / Personnalisation / Ne pas afficher l'�cran de v�rrouillage
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\Personalization" /v "NoLockScreen" /t REG_DWORD /d "1" /f

:: D�sactiver les menus contextuels large de Windows 10 en r�tablissant leur aspect classique
REG ADD "HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\FlightedFeatures" /v "ImmersiveContextMenu" /t REG_DWORD /d "0" /f

::---------------------------------------------------------------
ECHO.
ECHO Internet Explorer
::---------------------------------------------------------------
REG ADD "HKCU\SOFTWARE\Microsoft\Internet Explorer\Main" /v "UseThemes" /t REG_DWORD /d "0" /f
REG ADD "HKCU\SOFTWARE\Microsoft\Internet Explorer\Main" /v "Page_Transitions" /t REG_DWORD /d "0" /f
REG ADD "HKCU\SOFTWARE\Microsoft\Internet Explorer\Main" /v "SmoothScroll" /t REG_DWORD /d "0" /f
REG ADD "HKCU\SOFTWARE\Microsoft\Internet Explorer\Main" /v "Friendly http errors" /t REG_SZ /d "no" /f
REG ADD "HKCU\SOFTWARE\Microsoft\Internet Explorer\Main" /v "IEWatsonDisabled" /t REG_DWORD /d "1" /f
REG ADD "HKCU\Software\Microsoft\Internet Explorer\Main" /v "IEWatsonEnabled" /t REG_DWORD /d "0" /f
REG ADD "HKCU\Software\Microsoft\Internet Explorer\Main" /v "Use FormSuggest" /t REG_SZ /d "yes" /f
REG ADD "HKCU\SOFTWARE\Microsoft\Internet Explorer\TabbedBrowsing" /v "ShowTabsWelcome" /t REG_DWORD /d "0" /f
REG ADD "HKCU\SOFTWARE\Microsoft\Internet Explorer\TabbedBrowsing" /v "WarnOnClose" /t REG_DWORD /d "0" /f
REG ADD "HKCU\SOFTWARE\Microsoft\Internet Explorer\TabbedBrowsing" /v "NTPFirstRun" /t REG_DWORD /d "0" /f

:: Affichage des diverses barres d'outils
REG ADD "HKCU\SOFTWARE\Microsoft\Internet Explorer\MINIE" /v "AlwaysShowMenus" /t REG_DWORD /d "1" /f
REG ADD "HKCU\SOFTWARE\Microsoft\Internet Explorer\MINIE" /v "LinksBandEnabled" /t REG_DWORD /d "1" /f
REG ADD "HKCU\SOFTWARE\Microsoft\Internet Explorer\MINIE" /v "CommandBarEnabled" /t REG_DWORD /d "1" /f
REG ADD "HKCU\SOFTWARE\Microsoft\Internet Explorer\MINIE" /v "ShowStatusBar" /t REG_DWORD /d "1" /f
REG ADD "HKCU\SOFTWARE\Microsoft\Internet Explorer\MINIE" /v "ShowTabsBelowAddressBar" /t REG_DWORD /d "1" /f

:: Demander confirmation pour les t�l�chargements de fichiers
:: Gpedit.msc / Configuration Ordinateur / Mod�les d'administration / Composants Windows / Internet Explorer / Panneau de configuration internet / Onglet s�curit� / Demander confirmation pour les t�l�chargements de fichiers / activ�
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Internet Explorer\Restrictions" /v "AlwaysPromptWhenDownload" /t REG_DWORD /d "0" /f

:: Ne pas afficher le bouton Smiley 'Donnez-nous votre avis' de la barre d�outils
:: gpedit.msc / Configuration utilisateur / Mod�les d�administration / Composants Windows / Internet Explorer / Menu du navigateur / Menu�? (Aide)�: supprimer l�option de menu Envoyer des commentaires / Activ�
REG ADD "HKCU\Software\Policies\Microsoft\Internet Explorer\Restrictions" /v "NoHelpItemSendFeedback" /t REG_DWORD /d 1 /f

:: D�sactiver la g�olocalisation du navigateur 
:: gpedit.msc / Configuaration ordinateur / Mod�les d�administration / Composant Windows / Internet Explorer / D�sactiver la g�olocalisation du navigateur
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Internet Explorer\Geolocation" /v "PolicyDisableGeolocation" /t REG_DWORD /d 1 /f

:: Ne jamais laissez les sites Web demander votre emplacement (g�olocalisation)
REG ADD "HKCU\Software\Microsoft\Internet Explorer\Geolocation" /v "BlockAllWebsites" /t REG_DWORD /d "1" /f

:: Vider le dossier Fichiers Internet temporaires lorsque le navigateur est ferm�
:: Internet Explorer supprimera le contenu du dossier Fichiers Internet temporaires � la fermeture de toutes les fen�tres de navigation.
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\CurrentVersion\Internet Settings\Cache" /v "Persistent" /t REG_DWORD /d 0 /f

:: Effacer l'historique de navigation lors de la fermeture d'IE
REG ADD "HKCU\Software\Microsoft\Internet Explorer\Privacy" /v "ClearBrowsingHistoryOnExit" /t REG_DWORD /d "1" /f

:: Vider le dossier fichiers temporaires internet lorsque IE est ferm�
REG ADD "HKCU\SOFTWARE\Microsoft\Windows\CurrentVersion\Internet Settings\Cache" /v "Persistent" /t REG_DWORD /d "0" /f

:: Masquer le bouton (� c�t� du bouton Nouvel onglet) qui ouvre Microsoft Edge (RS2)  
REG ADD "HKCU\Software\Microsoft\Internet Explorer\Main" /v "HideNewEdgeButton" /t REG_DWORD /d 1 /f
::GPO
REG ADD "HKCU\Software\Policies\Microsoft\Internet Explorer\Main" /v "HideNewEdgeButon" /t REG_DWORD /d 1 /f

::Nombre maximal de connexions par serveur
:: defaut = 4
REG ADD "HKLM\SOFTWARE\Microsoft\Internet Explorer\Main\FeatureControl\FEATURE_MAXCONNECTIONSPER1_0SERVER" /v "explorer.exe" /t REG_DWORD /d "10" /f
REG ADD "HKLM\SOFTWARE\Microsoft\Internet Explorer\Main\FeatureControl\FEATURE_MAXCONNECTIONSPER1_0SERVER" /v "iexplorer.exe" /t REG_DWORD /d "10" /f
REG ADD "HKLM\SOFTWARE\WOW6432Node\Microsoft\Internet Explorer\Main\FeatureControl\FEATURE_MAXCONNECTIONSPER1_0SERVER" /v "explorer.exe" /t REG_DWORD /d "10" /f
REG ADD "HKLM\SOFTWARE\WOW6432Node\Microsoft\Internet Explorer\Main\FeatureControl\FEATURE_MAXCONNECTIONSPER1_0SERVER" /v "iexplore.exe" /t REG_DWORD /d "10" /f
:: d�faut = 2  (HTTP1)
REG ADD "HKLM\SOFTWARE\Microsoft\Internet Explorer\Main\FeatureControl\FEATURE_MAXCONNECTIONSPERSERVER" /v "explorer.exe" /t REG_DWORD /d "10" /f 
REG ADD "HKLM\SOFTWARE\Microsoft\Internet Explorer\Main\FeatureControl\FEATURE_MAXCONNECTIONSPERSERVER" /v "iexplorer.exe" /t REG_DWORD /d "10" /f 
REG ADD "HKLM\SOFTWARE\WOW6432Node\Microsoft\Internet Explorer\Main\FeatureControl\FEATURE_MAXCONNECTIONSPERSERVER" /v "explorer.exe" /t REG_DWORD /d "10" /f
REG ADD "HKLM\SOFTWARE\WOW6432Node\Microsoft\Internet Explorer\Main\FeatureControl\FEATURE_MAXCONNECTIONSPERSERVER" /v "iexplore.exe" /t REG_DWORD /d "10" /f


:: Moteur de recherhe Qwant par d�faut
reg delete "HKLM\Software\Microsoft\Internet Explorer\SearchScopes\{0633EE93-D776-472f-A0FF-E1416B8B2E3A}" /f
REG ADD "HKLM\Software\Microsoft\Internet Explorer\SearchScopes\{0633EE93-D776-472f-A0FF-E1416B8B2E3A}" /v "DisplayName" /t REG_SZ /d "Recherche sur Qwant" /f
REG ADD "HKCU\Software\Microsoft\Internet Explorer\SearchScopes" /v "DefaultScope" /t REG_SZ /d "{DA52178D-D8D8-46C2-B563-E48BAC872573}" /f
REG ADD "HKCU\Software\Microsoft\Internet Explorer\SearchScopes" /v "DownloadRetries" /t REG_DWORD /d "0" /f
REG ADD "HKCU\Software\Microsoft\Internet Explorer\SearchScopes" /v "Version" /t REG_DWORD /d "5" /f
REG ADD "HKCU\Software\Microsoft\Internet Explorer\SearchScopes" /v "DownloadUpdate" /t REG_DWORD /d "0" /f
REG ADD "HKCU\Software\Microsoft\Internet Explorer\SearchScopes" /v "ShowSearchSuggestionsInAddressGlobal" /t REG_DWORD /d "1" /f
REG ADD "HKCU\Software\Microsoft\Internet Explorer\SearchScopes" /v "UpgradeTime" /t REG_BINARY /d "881a4a7273fdd201" /f
REG ADD "HKCU\SOFTWARE\Microsoft\Internet Explorer\Main" /v "Search Page" /t REG_SZ /d "https://www.qwant.com" /f

:: Bing
REG ADD "HKCU\Software\Microsoft\Internet Explorer\SearchScopes\{0633EE93-D776-472f-A0FF-E1416B8B2E3A}" /v "Codepage" /t REG_DWORD /d "65001" /f
REG ADD "HKCU\Software\Microsoft\Internet Explorer\SearchScopes\{0633EE93-D776-472f-A0FF-E1416B8B2E3A}" /v "DisplayName" /t REG_SZ /d "Recherche sur Bing" /f
REG ADD "HKCU\Software\Microsoft\Internet Explorer\SearchScopes\{0633EE93-D776-472f-A0FF-E1416B8B2E3A}" /v "FaviconURL" /t REG_SZ /d "https://www.bing.com/favicon.ico" /f
REG ADD "HKCU\Software\Microsoft\Internet Explorer\SearchScopes\{0633EE93-D776-472f-A0FF-E1416B8B2E3A}" /v "ShowSearchSuggestions" /t REG_DWORD /d "1" /f
REG ADD "HKCU\Software\Microsoft\Internet Explorer\SearchScopes\{0633EE93-D776-472f-A0FF-E1416B8B2E3A}" /v "URL" /t REG_SZ /d "https://www.bing.com/search?q={searchTerms}&src=IE-SearchBox&FORM=IESR02" /f
REG ADD "HKCU\Software\Microsoft\Internet Explorer\SearchScopes\{0633EE93-D776-472f-A0FF-E1416B8B2E3A}" /v "SuggestionsURL" /t REG_SZ /d "https://api.bing.com/qsml.aspx?query={searchTerms}&market={language}&maxwidth={ie:maxWidth}&rowheight={ie:rowHeight}&sectionHeight={ie:sectionHeight}&FORM=IESS02" /f
REG ADD "HKCU\Software\Microsoft\Internet Explorer\SearchScopes\{0633EE93-D776-472f-A0FF-E1416B8B2E3A}" /v "NTURL" /t REG_SZ /d "https://www.bing.com/search?q={searchTerms}&src=IE-SearchBox&FORM=IENTSR" /f
REG ADD "HKCU\Software\Microsoft\Internet Explorer\SearchScopes\{0633EE93-D776-472f-A0FF-E1416B8B2E3A}" /v "NTTopResultURL" /t REG_SZ /d "https://www.bing.com/search?q={searchTerms}&src=IE-SearchBox&FORM=IENTTR" /f
REG ADD "HKCU\Software\Microsoft\Internet Explorer\SearchScopes\{0633EE93-D776-472f-A0FF-E1416B8B2E3A}" /v "NTSuggestionsURL" /t REG_SZ /d "https://api.bing.com/qsml.aspx?query={searchTerms}&market={language}&maxwidth={ie:maxWidth}&rowheight={ie:rowHeight}&sectionHeight={ie:sectionHeight}&FORM=IENTSS" /f
REG ADD "HKCU\Software\Microsoft\Internet Explorer\SearchScopes\{0633EE93-D776-472f-A0FF-E1416B8B2E3A}" /v "NTLogoURL" /t REG_SZ /d "https://go.microsoft.com/fwlink/?LinkID=403856&language={language}&scale={scalelevel}&contrast={contrast}" /f
:: Google
REG ADD "HKCU\Software\Microsoft\Internet Explorer\SearchScopes\{11EA010B-CA36-426C-BCD4-5D0E2D9006E8}" /v "DisplayName" /t REG_SZ /d "Recherche sur Google" /f
REG ADD "HKCU\Software\Microsoft\Internet Explorer\SearchScopes\{11EA010B-CA36-426C-BCD4-5D0E2D9006E8}" /v "OSDFileURL" /t REG_SZ /d "https://www.microsoft.com/fr-fr/IEGallery/GoogleAddOns" /f
REG ADD "HKCU\Software\Microsoft\Internet Explorer\SearchScopes\{11EA010B-CA36-426C-BCD4-5D0E2D9006E8}" /v "FaviconURL" /t REG_SZ /d "https://www.google.com/favicon.ico" /f
REG ADD "HKCU\Software\Microsoft\Internet Explorer\SearchScopes\{11EA010B-CA36-426C-BCD4-5D0E2D9006E8}" /v "ShowSearchSuggestions" /t REG_DWORD /d "1" /f
REG ADD "HKCU\Software\Microsoft\Internet Explorer\SearchScopes\{11EA010B-CA36-426C-BCD4-5D0E2D9006E8}" /v "URL" /t REG_SZ /d "https://www.google.com/search?q={searchTerms}&sourceid=ie7&rls=com.microsoft:{language}:{referrer:source}&ie={inputEncoding?}&oe={outputEncoding?}" /f
REG ADD "HKCU\Software\Microsoft\Internet Explorer\SearchScopes\{11EA010B-CA36-426C-BCD4-5D0E2D9006E8}" /v "SuggestionsURL" /t REG_SZ /d "https://www.google.com/complete/search?q={searchTerms}&client=ie8&mw={ie:maxWidth}&sh={ie:sectionHeight}&rh={ie:rowHeight}&inputencoding={inputEncoding}&outputencoding={outputEncoding}" /f
:: Startpage
REG ADD "HKCU\Software\Microsoft\Internet Explorer\SearchScopes\{89434952-2441-4D2B-9E55-DFC4D195D12B}" /v "Codepage" /t REG_DWORD /d "65001" /f
REG ADD "HKCU\Software\Microsoft\Internet Explorer\SearchScopes\{89434952-2441-4D2B-9E55-DFC4D195D12B}" /v "DisplayName" /t REG_SZ /d "Recherche sur Startpage HTTPS - Francais" /f
REG ADD "HKCU\Software\Microsoft\Internet Explorer\SearchScopes\{89434952-2441-4D2B-9E55-DFC4D195D12B}" /v "OSDFileURL" /t REG_SZ /d "https://www.startpage.com/toolbar/searchbar/fr/startpage_ie_secure_fr.src" /f
REG ADD "HKCU\Software\Microsoft\Internet Explorer\SearchScopes\{89434952-2441-4D2B-9E55-DFC4D195D12B}" /v "FaviconURL" /t REG_SZ /d "https://www.startpage.com/favicon.ico" /f
REG ADD "HKCU\Software\Microsoft\Internet Explorer\SearchScopes\{89434952-2441-4D2B-9E55-DFC4D195D12B}" /v "URL" /t REG_SZ /d "https://www.startpage.com/do/dsearch?query={searchTerms}&cat=web&pl=ie&language=francais" /f
:: Qwant
REG ADD "HKCU\Software\Microsoft\Internet Explorer\SearchScopes\{DA52178D-D8D8-46C2-B563-E48BAC872573}" /v "Codepage" /t REG_DWORD /d "65001" /f
REG ADD "HKCU\Software\Microsoft\Internet Explorer\SearchScopes\{DA52178D-D8D8-46C2-B563-E48BAC872573}" /v "DisplayName" /t REG_SZ /d "Recherche sur Qwant" /f
REG ADD "HKCU\Software\Microsoft\Internet Explorer\SearchScopes\{DA52178D-D8D8-46C2-B563-E48BAC872573}" /v "OSDFileURL" /t REG_SZ /d "https://www.qwant.com/opensearch.xml" /f
REG ADD "HKCU\Software\Microsoft\Internet Explorer\SearchScopes\{DA52178D-D8D8-46C2-B563-E48BAC872573}" /v "FaviconPath" /t REG_SZ /d "C:\Users\Administrateur\AppData\LocalLow\Microsoft\Internet Explorer\Services\search_{DA52178D-D8D8-46C2-B563-E48BAC872573}.ico" /f
REG ADD "HKCU\Software\Microsoft\Internet Explorer\SearchScopes\{DA52178D-D8D8-46C2-B563-E48BAC872573}" /v "FaviconURL" /t REG_SZ /d "https://www.qwant.com/favicon.ico" /f
REG ADD "HKCU\Software\Microsoft\Internet Explorer\SearchScopes\{DA52178D-D8D8-46C2-B563-E48BAC872573}" /v "SuggestionsURL_JSON" /t REG_SZ /d "https://api.qwant.com/api/suggest/?q={searchTerms}&client=opensearch" /f
REG ADD "HKCU\Software\Microsoft\Internet Explorer\SearchScopes\{DA52178D-D8D8-46C2-B563-E48BAC872573}" /v "ShowSearchSuggestions" /t REG_DWORD /d "1" /f
REG ADD "HKCU\Software\Microsoft\Internet Explorer\SearchScopes\{DA52178D-D8D8-46C2-B563-E48BAC872573}" /v "URL" /t REG_SZ /d "https://www.qwant.com/?q={searchTerms}&client=opensearch" /f

REG ADD "HKCU\SOFTWARE\Microsoft\Internet Explorer\Main" /v "Start Page_TIMESTAMP" /t REG_BINARY /d "a1da70072368d201" /f
REG ADD "HKCU\SOFTWARE\Microsoft\Internet Explorer\Main" /v "SyncHomePage Protected - It is a violation of Windows Policy to modify. See aka.ms/browserpolicy" /t REG_BINARY /d "" /f
REG ADD "HKCU\SOFTWARE\Microsoft\Internet Explorer\Main" /v "IE10RunOnceLastShown" /t REG_DWORD /d "1" /f
REG ADD "HKCU\SOFTWARE\Microsoft\Internet Explorer\Main" /v "IE10RunOnceLastShown_TIMESTAMP" /t REG_BINARY /d "039f750b2368d201" /f
REG ADD "HKCU\SOFTWARE\Microsoft\Internet Explorer\Main" /v "IE10TourShown" /t REG_DWORD /d "1" /f
REG ADD "HKCU\SOFTWARE\Microsoft\Internet Explorer\Main" /v "IE10TourNoShown" /t REG_DWORD /d "1" /f
REG ADD "HKCU\SOFTWARE\Microsoft\Internet Explorer\Main" /v "IE10TourShownTime" /t REG_BINARY /d "6f01780b2368d201" /f
:: Page d'accueil IE 
REG ADD "HKCU\SOFTWARE\Microsoft\Internet Explorer\Main" /v "Start Page" /t REG_SZ /d %iestartpage% /f


::---------------------------------------------------------------
ECHO.
ECHO R�seau
:: Voir doc chapitre 20.15 pour le param�trage
::---------------------------------------------------------------
:: D�sactiver LLMNR 
:: gpedit.msc : Configuration ordinateur / Mod�les d'administration / R�seau / Client DNS / D�sactiver la r�solution de noms multidiffusion (Activer)
:: LLMNR (Local Link Multicast Name Resolution) est un protocole de r�solution de noms secondaire. Les requ�tes sont envoy�es sur la liaison locale, un sous-r�seau simple, � partir d'un ordinateur client utilisant la multidiffusion auquel un autre client sur la m�me liaison, pour laquelle LLMNR est �galement activ�, peut r�pondre. LLMNR assure une r�solution de noms dans les sc�narios o� la r�solution de noms DNS classique est impossible.
REG ADD "HKLM\Software\Policies\Microsoft\Windows NT\DNSClient" /v "EnableMulticast" /t REG_DWORD /d "0" /f


:: D�sactive tous les composants IPv6, � l'exception de l'interface de bouclage IPv6. Cette valeur configure �galement Windows pour utiliser IPv4 plut�t que IPv6 en modifiant les entr�es dans cette table
:: https://support.microsoft.com/fr-fr/kb/929852
::REG ADD "HKLM\SYSTEM\ControlSet001\Services\Tcpip6\Parameters" /v "DisabledComponents" /t REG_DWORD /d "255" /f
REG ADD "HKLM\SYSTEM\currentControlSet\Services\Tcpip6\Parameters" /v "DisabledComponents" /t REG_DWORD /d "255" /f

:: D�sactiver les partages Administratifs invisibles Admin$, C$, IPC$, PRINT$, etc.
REG ADD "HKLM\SYSTEM\ControlSet001\services\LanmanServer\Parameters" /v "AutoShareWks" /t REG_DWORD /d "0" /f   	
REG ADD "HKLM\SYSTEM\ControlSet001\services\LanmanServer\Parameters" /v "AutoShareServer" /t REG_DWORD /d "0" /f

:: Param�trage optimisation DNS
:: (Recommand�:�4, d�faut�: 499) � local cache de noms
REG ADD "HKLM\SYSTEM\CurrentControlSet\Services\Tcpip\ServiceProvider" /v "LocalPriority" /t REG_DWORD /d "4" /f
:: (Recommand�:�5, d�faut�: 500) � le fichier HOSTS
REG ADD "HKLM\SYSTEM\CurrentControlSet\Services\Tcpip\ServiceProvider" /v "HostsPriority" /t REG_DWORD /d "5" /f 
:: (Recommand�:�6, d�faut�: 2000) � DNS
REG ADD "HKLM\SYSTEM\CurrentControlSet\Services\Tcpip\ServiceProvider" /v "DnsPriority" /t REG_DWORD /d "6" /f 
:: (Recommand�:�7, d�faut�: 2001) � r�solution de nom NetBT, incluant le WINS
REG ADD "HKLM\SYSTEM\CurrentControlSet\Services\Tcpip\ServiceProvider" /v "NetbtPriority" /t REG_DWORD /d "7" /f 

:: Param�ttre TTL
REG ADD "HKLM\System\CurrentControlSet\Services\Tcpip\Parameters" /v "DefaultTTL" /t REG_DWORD /d "64" /f 

:: R�glage auto de la fen�tre de r�ception (Default = normal)
Powershell Set-NetTCPSetting -SettingName InternetCustom -AutoTuningLevelLocal normal
:: Mise � l'�chelle de la fen�tre TCP Heuristque (Default = disable) 
Powershell Set-NetTCPSetting -SettingName InternetCustom -ScalingHeuristics disabled
:: forunisseur de ctrl de surcharge comp. (Compount TCP) (Default = disable)
Powershell Set-NetTCPSetting -SettingName InternetCustom -CongestionProvider ctcp
:: Fonctionnalit� ECN (default = disabled)
Powershell Set-NetTCPSetting -SettingName InternetCustom -EcnCapability disabled
:: Etat de d�chargement Chimney (default = disabled)
:: Powershell Set-NetOffloadGlobalSetting -Chimney disabled
:: Horodatage RFC 1323 (default = disabled)
Powershell Set-NetTCPSetting -SettingName InternetCustom -Timestamps disabled
:: Max SYN Retransmission (default = 2)
Powershell Set-NetTCPSetting -SettingName InternetCustom -MaxSynRetransmission 2
:: Non Stack RTT Resiliency (default = disabled)
Powershell Set-NetTCPSetting -SettingName InternetCustom -NonSackRttResiliency disabled
:: RTO initial (default = 3000)
Powershell Set-NetTCPSetting -SettingName InternetCustom -InitialRto 2000
:: RTO minimum (default = 300)
Powershell Set-NetTCPSetting -SettingName InternetCustom -MinRto 300
:: Acc�s au cache direct DCA (default = disabled)
netsh int tcp set global dca=enabled
:: RSS (Receive Side Scaling)
::Powershell Enable-NetAdapterRss -Name *
netsh int tcp set global rss=enabled
:: RSC (Receive Segment coalescing State)
::Powershell Enable-NetAdapterRsc -Name *
netsh int tcp set global rsc=enabled
:: Large Send Offeload (LSO) 
Powershell Disable-NetAdapterLso -Name *
:: Checksum offloading
Powershell Enable-NetAdapterChecksumOffload -Name *


:: D�sactive la bande passante r�servable QOS (Quality of Service)
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\Psched" /v "NonBestEffortLimit" /t REG_DWORD /d "0" /f
REG ADD "HKLM\SYSTEM\ControlSet001\Services\Tcpip\QoS" /v "Do not use NLA" /t REG_SZ /d "1" /f

:: Port d'allocation
REG ADD "HKLM\SYSTEM\ControlSet001\Services\Tcpip\Parameters" /v "TcpTimedWaitDelay" /t REG_DWORD /d "30" /f
REG ADD "HKLM\SYSTEM\ControlSet001\Services\LanmanServer\Parameters" /v "Size" /t REG_DWORD /d "3" /f

:: Permettre l'acc�s aux lecteurs r�seau des applications �lev�es en tant qu'administrateur
REG ADD "HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\System" /v "EnableLinkedConnections"  /t REG_DWORD /d "1" /f

::---------------------------------------------------------------
ECHO.
ECHO Optimisation
::---------------------------------------------------------------

:: Forcer Windows de d�charger les DLL de la m�moire
REG ADD "HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer" /v "AlwaysUnloadDLL" /t REG_DWORD /d "1" /f
REG ADD "HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\AlwaysUnloadDLL" /ve /t REG_DWORD /d "1" /f

:: Augmentation du cache des ic�nes pour un chargement plus rapide
REG ADD "HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer" /v "Max Cached Icons" /t REG_SZ /d "4096" /f

:: Fermer automatiquement les applications qui ne r�ponde pas pendant l'arr�t de la machine
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\System" /v "AllowBlockingAppsAtShutdown" /t REG_DWORD /d "0" /f

:: D�sactive l'animation � la premi�re ouverture de session
REG ADD "HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\System" /v "EnableFirstLogonAnimation" /t REG_DWORD /d "0" /f

:: gpedit.msc / Configuration Ordinateur / Mod�les d'administration / Composants Windows / Explorateur Windows /  D�sactiver le tri num�rique dans l'explorateur Windows (KB319827)
REG ADD "HKLM\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer" /v "NoStrCmpLogical" /t REG_DWORD /d "1" /f

:: gpedit.msc / Configuration Ordinateur / Mod�les d'administration / Composants Windows / Strat�gie de lecture automatique / D�sactiver le lecteur automatique (pour tous les lecteurs)(CDRom et support amovible = b5) (HKLM et HKCU)	  	
REG ADD "HKLM\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer" /v "NoDriveTypeAutoRun" /t REG_DWORD /d "255" /f

:: Accel�re le d�marrage des applications
REG ADD "HKCU\Software\Microsoft\Windows\CurrentVersion\Explorer\Serialize" /v "StartupDelayInMSec" /t REG_DWORD /d 75 /f
REG ADD "HKCU\Control Panel\Desktop" /v "MenuShowDelay" /t REG_DWORD /d 75 /f
REG ADD "HKCU\Control Panel\Mouse" /v "MouseHoverTime" /t REG_SZ /d 75 /f

::---------------------------------------------------------------
ECHO.
ECHO S�curit�
::---------------------------------------------------------------
:: Ne pas stocker le login et mot de passe sur le disque (defaut 10)
:: gpedit.msc / Configuration de l'ordinateur / Param�tres Windows / Param�tres de s�curit�s / Strat�gies locales / Options de s�curit� / Ouverture de session interactive�: nombre d�ouvertures de session ant�rieures � mettre en cache (au cas o� le contr�leur de domaine ne serait pas disponible) / 1
:: Poste nomade 5
REG ADD "HKLM\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon" /v "CachedLogonsCount" /t REG_SZ /d "1" /f

:: D�sactive l'option Rechercher sur internet lorsque un type de fichier n'est pas trouv� 
REG ADD "HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\Explorer" /v "NoInternetOpenWith" /t REG_DWORD /d "1" /f

:: Restreint l'acc�s aux utilisateurs anonymes (KB823659)
:: gpedit.msc / Configuration de l'ordinateur / Param�tres Windows / Param�tres de s�curit�s / Strat�gies locales / Options de s�curit� / Acc�s r�seau : ne pas autoriser l'�num�ration anonyme des comptes et partages SAM / Activ�
REG ADD "HKLM\SYSTEM\ControlSet001\Control\Lsa" /v "restrictanonymous" /t REG_DWORD /d "1" /f         
:: gpedit.msc / Configuration de l'ordinateur / Param�tres Windows / Param�tres de s�curit�s / Strat�gies locales / Options de s�curit� / Acc�s r�seau : ne pas autoriser l'�num�ration anonyme des comptes SAM / Activ�
REG ADD "HKLM\SYSTEM\ControlSet001\Control\Lsa" /v "restrictanonymoussam" /t REG_DWORD /d "1" /f  

:: Pour emp�cher le stockage en m�moire des informations d'identification de WDigest (KB2871997)
REG ADD "HKLM\System\ControlSet001\Control\SecurityProviders\WDigest" /v "UseLogonCredential" /t REG_DWORD /d "0" /f

:: Ne pas stocker de valeurs de hachage de niveau LAN Manager
:: gpedit.msc / Configuration de l�ordinateur / Param�tres Windows / Param�tres de s�curit�s / Strat�gies locales / Options de s�curit� / S�curit� r�seau�: ne pas stocker de valeurs de hachage de niveau LAN Manager sur la prochaine modification de mot de passe / Activ�
REG ADD "HKLM\SYSTEM\ControlSet001\Control\Lsa" /v "NoLmHash" /t REG_DWORD /d "1" /f

::---------------------------------------------------------------
ECHO.
ECHO Syst�me 
::---------------------------------------------------------------
:: Am�lioration des performances du syst�me de fichier NTFS
:: http://technet.microsoft.com/fr-fr/library/cc785435.aspx
:: Eviter la cr�ation des noms sous le mode DOS 8.3 et la fragmentation inutile de la table MTF 'KB210638'
REG ADD "HKLM\SYSTEM\ControlSet001\Control\FileSystem" /v "NtfsDisable8dot3NameCreation" /t REG_DWORD /d "1" /f
fsutil 8dot3name set 1
:: Permet la compression NTFS (0) Active ou d�sactive (1) .
REG ADD "HKLM\SYSTEM\ControlSet001\Control\FileSystem" /v "NtfsDisableCompression" /t REG_DWORD /d "1" /f				 
fsutil.exe behavior set DisableCompression 1
:: D�sactive (1) ou Active (0) le cryptage des dossiers et fichiers sur des volumes NTFS.
REG ADD "HKLM\SYSTEM\ControlSet001\Control\FileSystem" /v "NtfsDisableEncryption" /t REG_DWORD /d "1" /f		
fsutil.exe behavior set DisableEncryption 1	  
:: D�sactive (1)  la mise � jour de la date de dernier acc�s des fichiers sur un volume NTFS (defaut).Ce param�tre peut affecter des programmes tels que la sauvegarde et le stockage �tendu qui reposent sur cette fonctionnalit�.
REG ADD "HKLM\SYSTEM\ControlSet001\Control\FileSystem" /v "NtfsDisableLastAccessUpdate" /t REG_DWORD /d "1" /f
fsutil behavior set DisableLastAccess 1			
:: Configure les niveaux de cache interne de m�moire pagin�e et non pagin�e du NTFS (0 = default, 1 � 2 = max)
REG ADD "HKLM\SYSTEM\ControlSet001\Control\FileSystem" /v "NtfsMemoryUsage" /t REG_DWORD /d "2" /f	
fsutil.exe behavior set MemoryUsage 2
:: D�sactive la fragmentation de la MFT: De cette fa�on la MFT ne se fragmentera pas et ne ralentira pas votre syst�mer. (defaut=0) (1=200Mo 2=400Mo 3=600Mo 4=800Mo d'espace reserv�)
REG ADD "HKLM\SYSTEM\ControlSet001\Control\FileSystem" /v "NtfsMftZoneReservation" /t REG_DWORD /d "2" /f				
fsutil.exe behavior set MftZone 2

:: D�sactiver le cache des miniatures (Thumb.db) 
REG ADD "HKCU\Software\Microsoft\Windows\CurrentVersion\Policies" /v "NoThumbnailCache" /t REG_DWORD /d "1" /f
REG ADD "HKCU\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Advanced" /v "NoThumbnailCache" /t REG_DWORD /d "1" /f
REG ADD "HKCU\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Advanced" /v "DisableThumbnailCache" /t REG_DWORD /d "1" /f
REG ADD "HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\Explorer" /v "NoThumbnailCache" /t REG_DWORD /d "1" /f
REG ADD "HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\Explorer" /v "DisableThumbnailCache" /t REG_DWORD /d "1" /f

:: D�sactiver le cache des miniatures (Thumb.db) sur les lecteurs r�seau
:: gpedit.msc / Configuration Utilisateur / Mod�les d'administration / Composant Windows / Explorateur de fichier / D�sactiver l'affichage des miniatures et affichier seulement les ic�nes sur les dossiers r�seau / Activ�
REG ADD "HKCU\SOFTWARE\Policies\Microsoft\Windows\Explorer" /v "DisableThumbsDBOnNetworkFolders" /t REG_DWORD /d "1" /f

:: Afficher le d�tail d'un BSOD � la place du smiley 
REG ADD "HKLM\System\CurrentControlSet\Control\CrashControl" /v "DisplayParameters " /t REG_DWORD /d "1" /f

::---------------------------------------------------------------
ECHO.
ECHO Windows Update 
::---------------------------------------------------------------
:: Sp�cifier l�emplacement intranet du service de mise � jour Microsoft (WSUS)
:: gpedit.msc / Configuration Ordinateur / Mod�les d'administration / Composant Windows / Windows Update / Specifier l'emplacement intranet du service de mise � jour Microsoft 
reg add "HKLM\Software\Policies\Microsoft\Windows\WindowsUpdate" /v "WUServer" /t REG_SZ /d %wsus% /f
reg add "HKLM\Software\Policies\Microsoft\Windows\WindowsUpdate" /v "WUStatusServer" /t REG_SZ /d %wsus% /f
reg add "HKLM\Software\Policies\Microsoft\Windows\WindowsUpdate" /v "UpdateServiceUrlAlternate" /t REG_SZ /d "" /f
reg add "HKLM\Software\Policies\Microsoft\Windows\WindowsUpdate\AU" /v "UseWUServer" /t REG_DWORD /d "1" /f

:: gpedit.msc / Configuration Ordinateur / Mod�les d'administration / Composant Windows / Windows Update / Configuration du service de mise � jour (0=Active la mise � jour automatique, 1=d�sactive)
reg add "HKLM\Software\Policies\Microsoft\Windows\WindowsUpdate\AU" /v "NoAutoUpdate" /t REG_DWORD /d "0" /f
:: gpedit.msc / Configuration Ordinateur / Mod�les d'administration / Composant Windows / Windows Update / Configuration du service de mise � jour (4=T�l�charger automatiquement les mises � jour et les installer en fonction de la planification)
reg add "HKLM\Software\Policies\Microsoft\Windows\WindowsUpdate\AU" /v "AUOptions" /t REG_DWORD /d "4" /f
:: gpedit.msc / Configuration Ordinateur / Mod�les d'administration / Composant Windows / Windows Update / Configuration du service de mise � jour (0=Tous les jours, 1=dimanche,2=Lundi,3=mardi,...)
reg add "HKLM\Software\Policies\Microsoft\Windows\WindowsUpdate\AU" /v "ScheduledInstallDay" /t REG_DWORD /d "0" /f
:: gpedit.msc / Configuration Ordinateur / Mod�les d'administration / Composant Windows / Windows Update / Configuration du service de mise � jour (l'heure de d�clanchement ici a 12 heures)
reg add "HKLM\Software\Policies\Microsoft\Windows\WindowsUpdate\AU" /v "ScheduledInstallTime" /t REG_DWORD /d "12" /f

:: gpedit.msc / configuration ordinateur / Mod�les d�administration / Composants Windows / Windows update / Diff�rer les mises � jour Windows /  Choisir quand recevoir les mises � jours des fonctionnalit�s : Activ� 
:: Current Branch for Business - 120 jrs
reg add "HKLM\SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate" /v "DeferFeatureUpdates" /t REG_DWORD /d "1" /f 
reg add "HKLM\SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate" /v "BranchReadinessLevel" /t REG_DWORD /d "32" /f 
reg add "HKLM\SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate" /v "DeferFeatureUpdatesPeriodInDays" /t REG_DWORD /d "120" /f 
:: reg add "HKLM\SOFTWARE\Microsoft\WindowsUpdate\UX\Settings" /v "DeferUpgrade" /t REG_DWORD /d "1" /f 
:: Contourner la connexion apr�s une mise � jour de Windows�10
reg add "HKLM\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon" /v "ARSOUserConsent" /t REG_DWORD /d "1" /f


::---------------------------------------------------------------
ECHO.
ECHO Serveur NTP
::---------------------------------------------------------------
REG ADD "HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\DateTime\Servers" /ve /t REG_SZ /d "3" /f
REG ADD "HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\DateTime\Servers" /v "3" /t REG_SZ /d %ntp% /f

::---------------------------------------------------------------
ECHO.
ECHO Services
::---------------------------------------------------------------
:: Assistant Connexion avec un compte Microsoft (ne pas d�sactiver dans un master sous peine de ne pas pouvoir activer la machine)
:: REG ADD "HKLM\SYSTEM\ControlSet001\Services\wlidsvc" /v start /t REG_DWORD /d 4 /f
:: BranchCache
REG ADD "HKLM\SYSTEM\ControlSet001\Services\PeerDistSvc" /v start /t REG_DWORD /d 4 /f
:: Dossier de travail
REG ADD "HKLM\SYSTEM\ControlSet001\Services\workfolderssvc" /v start /t REG_DWORD /d 4 /f
:: Gestion � distance de Windows (Gestion WSM)
REG ADD "HKLM\SYSTEM\ControlSet001\Services\WinRM" /v start /t REG_DWORD /d 4 /f
:: Gestionnaire d�authentification Xbox Live
REG ADD "HKLM\SYSTEM\ControlSet001\Services\XblAuthManager" /v start /t REG_DWORD /d 4 /f
:: Interface de services d�invit� Hyper-V
REG ADD "HKLM\SYSTEM\ControlSet001\Services\vmicguestinterface" /v start /t REG_DWORD /d 4 /f
:: Jeu sauvegard� sur xbox live
REG ADD "HKLM\SYSTEM\ControlSet001\Services\XblGameSave" /v start /t REG_DWORD /d 4 /f
:: Localisateur d�appels de proc�dure distante (RPC)
REG ADD "HKLM\SYSTEM\ControlSet001\Services\RpcLocator" /v start /t REG_DWORD /d 4 /f
:: Requ�te du service VSS Microsoft Hyper-V
REG ADD "HKLM\SYSTEM\ControlSet001\Services\vmicvss" /v start /t REG_DWORD /d 4 /f
:: Service Arr�t de l�invit� Microsoft Hyper-V
REG ADD "HKLM\SYSTEM\ControlSet001\Services\vmicshutdown" /v start /t REG_DWORD /d 4 /f
:: Service Broker pour les �v�nements horaires
REG ADD "HKLM\SYSTEM\ControlSet001\Services\TimeBrokerSvc" /v start /t REG_DWORD /d 4 /f
:: Service de capteur
REG ADD "HKLM\SYSTEM\ControlSet001\Services\SensorService" /v start /t REG_DWORD /d 4 /f
:: Service de gestion des applications d�entreprise
REG ADD "HKLM\SYSTEM\ControlSet001\Services\EntAppSvc" /v start /t REG_DWORD /d 4 /f
:: Service de mise en r�seau Xbox Live
REG ADD "HKLM\SYSTEM\ControlSet001\Services\XboxNetApiSvc" /v start /t REG_DWORD /d 4 /f
:: Service de surveillance des capteurs
REG ADD "HKLM\SYSTEM\ControlSet001\Services\SensrSvc" /v start /t REG_DWORD /d 4 /f
:: Service de Virtualisation Bureau � distance Hyper-V
REG ADD "HKLM\SYSTEM\ControlSet001\Services\XboxNetApiSvc" /v start /t REG_DWORD /d 4 /f
:: Service donn�es de capteur
REG ADD "HKLM\SYSTEM\ControlSet001\SensorDataService" /v start /t REG_DWORD /d 4 /f
:: Service �change de donn�es Microsoft Hyper-V
REG ADD "HKLM\SYSTEM\ControlSet001\Services\vmickvpexchange" /v start /t REG_DWORD /d 4 /f
:: Service Hyper-V Powershell Direct
REG ADD "HKLM\SYSTEM\ControlSet001\Services\vmicvmsession" /v start /t REG_DWORD /d 4 /f
:: Service Initiateur iSCSI de Microsoft
REG ADD "HKLM\SYSTEM\ControlSet001\Services\MSiSCSI" /v start /t REG_DWORD /d 4 /f
:: Service Partage r�seau du Lecteur Windows Media
REG ADD "HKLM\SYSTEM\ControlSet001\Services\WMPNetworkSvc" /v start /t REG_DWORD /d 4 /f
:: Service Point d�acc�s sans fil mobile Windows
REG ADD "HKLM\SYSTEM\ControlSet001\Services\icssvc" /v start /t REG_DWORD /d 4 /f
:: Service pulsation Microsoft Hyper-V
REG ADD "HKLM\SYSTEM\ControlSet001\Services\vmicheartbeat" /v start /t REG_DWORD /d 4 /f
:: Service Router SMS Microsoft Windows
REG ADD "HKLM\SYSTEM\ControlSet001\Services\SmsRouter" /v start /t REG_DWORD /d 4 /f
:: Service Synchronisation date/heure Microsoft Hyper-V
REG ADD "HKLM\SYSTEM\ControlSet001\Services\vmictimesync" /v start /t REG_DWORD /d 4 /f
:: Service Windows Insider*****
REG ADD "HKLM\SYSTEM\ControlSet001\Services\wisvc" /v start /t REG_DWORD /d 4 /f
:: T�l�copie **
REG ADD "HKLM\SYSTEM\ControlSet001\Services\Fax" /v start /t REG_DWORD /d 4 /f
:: Windows Search
REG ADD "HKLM\SYSTEM\ControlSet001\Services\WSearch" /v start /t REG_DWORD /d 4 /f 


::---------------------------------------------------------------
ECHO.
ECHO Cr�ation d'un Active Setup
::---------------------------------------------------------------
REG ADD "HKLM\SOFTWARE\Microsoft\Active Setup\Installed Components\{870A39A1-BAA8-43A2-B9AF-57D5D1AD9BF6}" /v "StubPath" /t REG_SZ /d "cmd /c \"\"%%Public%%\ConfTelemetrie\reg_activesetup.bat\"\"" /f
REG ADD "HKLM\SOFTWARE\Microsoft\Active Setup\Installed Components\{870A39A1-BAA8-43A2-B9AF-57D5D1AD9BF6}" /v "Version" /t REG_SZ /d "1,0,0" /f
REG ADD "HKLM\SOFTWARE\Microsoft\Active Setup\Installed Components\{870A39A1-BAA8-43A2-B9AF-57D5D1AD9BF6}" /ve /t REG_SZ /d "Blocage T�l�m�trie" /f

::---------------------------------------------------------------
ECHO. SAMBA 3
::---------------------------------------------------------------
::samba
REG ADD "HKLM\SYSTEM\ControlSet001\Services\LanmanWorkstation\Parameters" /v "DNSNameResolutionRequired" /t REG_DWORD /d 0 /f > NUL 2>&1
REG ADD "HKLM\SYSTEM\ControlSet001\Services\LanmanWorkstation\Parameters" /v "DomainCompatibilityMode" /t REG_DWORD /d 1 /f	> NUL 2>&1
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\NetworkProvider\HardenedPaths" /v "\\*\netlogon" /t REG_SZ /d "RequireMutualAuthentication=0,RequireIntegrity=0,RequirePrivacy=0" /f > NUL 2>&1

:: Param�tres d'optimisation Samba
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\System" /v "SlowLinkDetectEnabled" /t REG_DWORD /d 0 /f  > NUL 2>&1
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\System" /v "DeleteRoamingCache" /t REG_DWORD /d 1 /f  > NUL 2>&1
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\System" /v "WaitForNetwork" /t REG_DWORD /d 0 /f > NUL 2>&1
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\System" /v "CompatibleRUPSecurity" /t REG_DWORD /d 1 /f  > NUL 2>&1

::---------------------------------------------------------------
ECHO. Param�trage UAC
::---------------------------------------------------------------
:: gpedit.msc / Configuration Ordinateur / Param�tres Windows / Param�tres de s�curit� / Strat�gies locales / Options de s�curit� / Contr�le de compte d'utilisateur : d�tecter les installations d'applications et demander l'�l�vation (d�sactiv�) d�faut = 1
REG ADD "HKLM\Software\Microsoft\Windows\CurrentVersion\Policies\System" /v "EnableInstallerDetection" /t REG_DWORD /d 0 /f
:: gpedit.msc / Configuration Ordinateur / Param�tres Windows / Param�tres de s�curit� / Strat�gies locales / Options de s�curit� / Contr�le de compte d'utilisateur / Comportement de l'invite d'�l�vation pour les administrateurs en mode d'approbation Administrateur / Demande de consentement sur le bureau s�curis� - default = Demande de consentement pour les binaires non Windows (defaut=5)
REG ADD "HKLM\Software\Microsoft\Windows\CurrentVersion\Policies\System" /v "ConsentPromptBehaviorAdmin" /t REG_DWORD /d 5 /f
:: gpedit.msc / Configuration Ordinateur / Param�tres Windows / Param�tres de s�curit� / Strat�gies locales / Options de s�curit� / Contr�le de compte d'utilisateur : Comportement de l�invite d��l�vation pour les utilisateurs standards / Refuser automatiquement les demandes d'�l�vation de privil�ges
REG ADD "HKLM\Software\Microsoft\Windows\CurrentVersion\Policies\System" /v "ConsentPromptBehaviorUser" /t REG_DWORD /d 0 /f
:: gpedit.msc / Configuration Ordinateur / Param�tres Windows / Param�tres de s�curit� / Strat�gies locales / Options de s�curit� / Contr�le de compte d'utilisateur : Ex�cuter les comptes d'administrateurs en mode d'approbation d'administrateur (Activ�) d�faut = 1
REG ADD "HKLM\Software\Microsoft\Windows\CurrentVersion\Policies\System" /v  "EnableLUA" /t REG_DWORD /d 1 /f
:: gpedit.msc / Configuration Ordinateur / Param�tres Windows / Param�tres de s�curit� / Strat�gies locales / Options de s�curit� / Contr�le de compte d'utilisateur : Mode Approbation administrateur pour le compte Administrateur int�gr� (Activ�) d�faut=0 (d�sactiv�)
REG ADD "HKLM\Software\Microsoft\Windows\CurrentVersion\Policies\System" /v "FilterAdministratorToken" /t REG_DWORD /d 1 /f
:: C�est la valeur par d�faut. Les informations d�identification d�administrateur sont supprim�es. Ces informations d�identification sont requises pour l�administration � distance des pilotes d�imprimante. d�faut=0
REG ADD "HKLM\Software\Microsoft\Windows\CurrentVersion\Policies\System" /v "LocalAccountTokenFilterPolicy" /t REG_DWORD /d 0 /f
:: gpedit.msc / Configuration Ordinateur / Param�tres Windows / Param�tres de s�curit� / Strat�gies locales / Options de s�curit� / Contr�le de compte d'utilisateur : 
:: D�sactive le bureau sombre s�curis� en cas de rel�vement du niveau de s�curit� 
REG ADD "HKLM\Software\Microsoft\Windows\CurrentVersion\Policies\System" /v "PromptOnSecureDesktop" /t REG_DWORD /d 0 /f


::---------------------------------------------------------------
ECHO. Jeux
::---------------------------------------------------------------
:: D�sactiver la barre de jeu (RS2)
REG ADD "HKCU\System\GameConfigStore" /v "GameDVR_Enabled" /t REG_DWORD /d 0 /f


::---------------------------------------------------------------
ECHO. DIVERS
::---------------------------------------------------------------
:: Si vous voulez les commandes DOS � la place de powershell
REG ADD "HKCU\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced" /v "DontUsePowerShellOnWinX" /t REG_DWORD /d 1 /f

:: Utiliser la saisie semi-automatique dans la barre d�adresse de l�explorateur de fichier et de la boite de dialogue Ex�cuter.
REG ADD "HKCU\Software\Microsoft\Windows\CurrentVersion\Explorer\AutoComplete" /v  "Append Completion" /t REG_SZ /d "yes" /f


:: GPEDIT / Configuration Ordinateur / Mod�les d'administration / Syst�me / Gestion de la communication Internet /  Param�tres de communication Internet / D�sactiver l'option Publier sur le Web de la gestion des fichiers : activ�
REG ADD "HKLM\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer" /v "NoPublishingWizard" /t REG_DWORD /d 1 /f


:: GPEDIT / Configuration Ordinateur / Mod�les d'administration / Composant Windows / Processus d'ajout de fonctionnalit�s � Windows 10 / Emp�cher l'ex�cution de l'assistant : activ�
REG ADD "HKLM\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer\WAU" /v "Disabled" /t REG_DWORD /d 1 /f


::----------------------------------------------------------------
ECHO Messenger
::----------------------------------------------------------------
:: GPEDIT / Configuration Ordinateur / Mod�les d'administration / Composant Windows / Windows Messenger / Ne pas d�marrer initialement Windows Messenger de mani�re automatique : activ�
REG ADD "HKLM\Software\Policies\Microsoft\Messenger\Client" /v "PreventAutoRun" /t REG_DWORD /d 1 /f

:: GPEDIT / Configuration Ordinateur / Mod�les d'administration / Composant Windows / Windows Messenger / Ne pas autoriser l'ex�cution de Windows Messenger: activ�
REG ADD "HKLM\Software\Policies\Microsoft\Messenger\Client" /v "PreventRun" /t REG_DWORD /d 1 /f

:: GPEDIT / Configuration Ordinateur / Mod�les d'administration / Syst�me /  Gestion de la communication Internet /  Param�tres de communication Internet / D�sactiver le programme d'am�lioration des services pour windows messenger
REG ADD "HKLM\Software\Policies\Microsoft\Messenger\Client" /v "CEIP" /t REG_DWORD /d 1 /f

::----------------------------------------------------------------
ECHO EDGE
::----------------------------------------------------------------
REG ADD "HKCU\Software\Classes\Local Settings\Software\Microsoft\Windows\CurrentVersion\AppContainer\Storage\microsoft.microsoftedge_8wekyb3d8bbwe\MicrosoftEdge\Main" /v "IE10TourNoShown" /t REG_DWORD /d "1" /f
REG ADD "HKCU\Software\Classes\Local Settings\Software\Microsoft\Windows\CurrentVersion\AppContainer\Storage\microsoft.microsoftedge_8wekyb3d8bbwe\MicrosoftEdge\Main" /v "IE10TourShown" /t REG_DWORD /d "1" /f
REG ADD "HKCU\Software\Classes\Local Settings\Software\Microsoft\Windows\CurrentVersion\AppContainer\Storage\microsoft.microsoftedge_8wekyb3d8bbwe\MicrosoftEdge\Main" /v "DoNotTrack" /t REG_DWORD /d "1" /f

::----------------------------------------------------------------
ECHO Clavier
::----------------------------------------------------------------
:: D�sactive l'animation de fen�tre lors du redimensionnement
REG ADD "HKCU\Control Panel\Desktop\WindowMetrics" /v "MinAnimate" /t REG_SZ /d "0" /f

:: Activer automatiquement le pav� num�rique
REG ADD "HKCU\Control Panel\Keyboard" /v "InitialKeyboardIndicators" /t REG_SZ /d "2" /f
REG ADD "HKU\.Default\Control Panel\Keyboard" /v "InitialKeyboardIndicators" /t REG_SZ /d "2" /f
REG ADD "HKU\S-1-5-18\Control Panel\Keyboard" /v "InitialKeyboardIndicators" /t REG_SZ /d "2" /f

REG ADD "HKCU\Control Panel\Mouse" /v "MouseHoverTime" /t REG_SZ /d "10" /f
pause