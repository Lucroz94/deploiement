@echo off

::===========================================================================================================
ECHO. 4.1 - Suppression des applications par d�faut (sauf Store, Calculatrice et StickyNotes)
::===========================================================================================================
Powershell -command Remove-AppxPackage -Package Microsoft.3DBuilder_13.0.10349.0_neutral_~_8wekyb3d8bbwe 
Powershell -command Remove-AppxProvisionedPackage -Online -PackageName Microsoft.3DBuilder_13.0.10349.0_neutral_~_8wekyb3d8bbwe
:: 3D Builder
REG DELETE "HKLM\Software\Classes\SystemFileAssociations\.bmp\Shell\3D Edit" /f
REG DELETE "HKLM\Software\Classes\SystemFileAssociations\.jpg\Shell\3D Edit" /f
REG DELETE "HKLM\Software\Classes\SystemFileAssociations\.png\Shell\3D Edit" /f

Powershell -command Remove-AppxPackage -Package Microsoft.BingWeather_4.18.56.0_neutral_~_8wekyb3d8bbwe 
Powershell -command Remove-AppxProvisionedPackage -Online -PackageName Microsoft.BingWeather_4.18.56.0_neutral_~_8wekyb3d8bbwe 
Powershell -command Remove-AppxPackage -Package Microsoft.DesktopAppInstaller_1.1.25002.0_neutral_~_8wekyb3d8bbwe 
Powershell -command Remove-AppxProvisionedPackage -Online -PackageName Microsoft.DesktopAppInstaller_1.1.25002.0_neutral_~_8wekyb3d8bbwe 
Powershell -command Remove-AppxPackage -Package Microsoft.Getstarted_4.5.6.0_neutral_~_8wekyb3d8bbwe 
Powershell -command Remove-AppxProvisionedPackage -Online -PackageName Microsoft.Getstarted_4.5.6.0_neutral_~_8wekyb3d8bbwe 
Powershell -command Remove-AppxPackage -Package Microsoft.Messaging_3.2.24002.0_neutral_~_8wekyb3d8bbwe 
Powershell -command Remove-AppxProvisionedPackage -Online -PackageName Microsoft.Messaging_3.2.24002.0_neutral_~_8wekyb3d8bbwe 
Powershell -command Remove-AppxPackage -Package Microsoft.Microsoft3DViewer_1.1702.21039.0_neutral_~_8wekyb3d8bbwe 
Powershell -command Remove-AppxProvisionedPackage -Online -PackageName Microsoft.Microsoft3DViewer_1.1702.21039.0_neutral_~_8wekyb3d8bbwe 
Powershell -command Remove-AppxPackage -Package Microsoft.MicrosoftOfficeHub_2017.311.255.0_neutral_~_8wekyb3d8bbwe 
Powershell -command Remove-AppxProvisionedPackage -Online -PackageName Microsoft.MicrosoftOfficeHub_2017.311.255.0_neutral_~_8wekyb3d8bbwe 
Powershell -command Remove-AppxPackage -Package Microsoft.MicrosoftSolitaireCollection_3.14.1181.0_neutral_~_8wekyb3d8bbwe 
Powershell -command Remove-AppxProvisionedPackage -Online -PackageName Microsoft.MicrosoftSolitaireCollection_3.14.1181.0_neutral_~_8wekyb3d8bbwe 
Powershell -command Remove-AppxPackage -Package Microsoft.MSPaint_1.1702.28017.0_neutral_~_8wekyb3d8bbwe 
Powershell -command Remove-AppxProvisionedPackage -Online -PackageName Microsoft.MSPaint_1.1702.28017.0_neutral_~_8wekyb3d8bbwe 
Powershell -command Remove-AppxPackage -Package Microsoft.Office.OneNote_2015.7668.58071.0_neutral_~_8wekyb3d8bbwe 
Powershell -command Remove-AppxProvisionedPackage -Online -PackageName Microsoft.Office.OneNote_2015.7668.58071.0_neutral_~_8wekyb3d8bbwe 
Powershell -command Remove-AppxPackage -Package Microsoft.OneConnect_2.1701.277.0_neutral_~_8wekyb3d8bbwe 
Powershell -command Remove-AppxProvisionedPackage -Online -PackageName Microsoft.OneConnect_2.1701.277.0_neutral_~_8wekyb3d8bbwe 
Powershell -command Remove-AppxPackage -Package Microsoft.People_2017.222.1920.0_neutral_~_8wekyb3d8bbwe 
Powershell -command Remove-AppxProvisionedPackage -Online -PackageName Microsoft.People_2017.222.1920.0_neutral_~_8wekyb3d8bbwe 
Powershell -command Remove-AppxPackage -Package Microsoft.SkypeApp_11.8.204.0_neutral_~_kzf8qxf38zg5c 
Powershell -command Remove-AppxProvisionedPackage -Online -PackageName Microsoft.SkypeApp_11.8.204.0_neutral_~_kzf8qxf38zg5c 
Powershell -command Remove-AppxPackage -Package Microsoft.StorePurchaseApp_1.0.454.0_neutral_~_8wekyb3d8bbwe 
Powershell -command Remove-AppxProvisionedPackage -Online -PackageName Microsoft.StorePurchaseApp_1.0.454.0_neutral_~_8wekyb3d8bbwe 
Powershell -command Remove-AppxPackage -Package Microsoft.Wallet_1.0.16328.0_neutral_~_8wekyb3d8bbwe 
Powershell -command Remove-AppxProvisionedPackage -Online -PackageName Microsoft.Wallet_1.0.16328.0_neutral_~_8wekyb3d8bbwe 
Powershell -command Remove-AppxPackage -Package Microsoft.WindowsAlarms_2017.203.236.0_neutral_~_8wekyb3d8bbwe 
Powershell -command Remove-AppxProvisionedPackage -Online -PackageName Microsoft.WindowsAlarms_2017.203.236.0_neutral_~_8wekyb3d8bbwe 
Powershell -command Remove-AppxPackage -Package Microsoft.WindowsCamera_2017.125.40.0_neutral_~_8wekyb3d8bbwe 
Powershell -command Remove-AppxProvisionedPackage -Online -PackageName Microsoft.WindowsCamera_2017.125.40.0_neutral_~_8wekyb3d8bbwe 
Powershell -command Remove-AppxPackage -Package microsoft.windowscommunicationsapps_2015.7906.42257.0_neutral_~_8wekyb3d8bbwe 
Powershell -command Remove-AppxProvisionedPackage -Online -PackageName microsoft.windowscommunicationsapps_2015.7906.42257.0_neutral_~_8wekyb3d8bbwe 
Powershell -command Remove-AppxPackage -Package Microsoft.WindowsFeedbackHub_1.1612.10312.0_neutral_~_8wekyb3d8bbwe 
Powershell -command Remove-AppxProvisionedPackage -Online -PackageName Microsoft.WindowsFeedbackHub_1.1612.10312.0_neutral_~_8wekyb3d8bbwe 
Powershell -command Remove-AppxPackage -Package Microsoft.WindowsMaps_2017.209.105.0_neutral_~_8wekyb3d8bbwe 
Powershell -command Remove-AppxProvisionedPackage -Online -PackageName Microsoft.WindowsMaps_2017.209.105.0_neutral_~_8wekyb3d8bbwe 
Powershell -command Remove-AppxPackage -Package Microsoft.WindowsSoundRecorder_2017.130.1208.0_neutral_~_8wekyb3d8bbwe 
Powershell -command Remove-AppxProvisionedPackage -Online -PackageName Microsoft.WindowsSoundRecorder_2017.130.1208.0_neutral_~_8wekyb3d8bbwe 
Powershell -command Remove-AppxPackage -Package Microsoft.XboxApp_2017.113.1250.0_neutral_~_8wekyb3d8bbwe 
Powershell -command Remove-AppxProvisionedPackage -Online -PackageName Microsoft.XboxApp_2017.113.1250.0_neutral_~_8wekyb3d8bbwe 
Powershell -command Remove-AppxPackage -Package Microsoft.XboxGameOverlay_1.15.2003.0_neutral_~_8wekyb3d8bbwe 
Powershell -command Remove-AppxProvisionedPackage -Online -PackageName Microsoft.XboxGameOverlay_1.15.2003.0_neutral_~_8wekyb3d8bbwe 
Powershell -command Remove-AppxPackage -Package Microsoft.XboxIdentityProvider_2016.719.1035.0_neutral_~_8wekyb3d8bbwe 
Powershell -command Remove-AppxProvisionedPackage -Online -PackageName Microsoft.XboxIdentityProvider_2016.719.1035.0_neutral_~_8wekyb3d8bbwe 
Powershell -command Remove-AppxPackage -Package Microsoft.XboxSpeechToTextOverlay_1.14.2002.0_neutral_~_8wekyb3d8bbwe 
Powershell -command Remove-AppxProvisionedPackage -Online -PackageName Microsoft.XboxSpeechToTextOverlay_1.14.2002.0_neutral_~_8wekyb3d8bbwe 
Powershell -command Remove-AppxPackage -Package Microsoft.ZuneMusic_2019.16112.11621.0_neutral_~_8wekyb3d8bbwe 
Powershell -command Remove-AppxProvisionedPackage -Online -PackageName Microsoft.ZuneMusic_2019.16112.11621.0_neutral_~_8wekyb3d8bbwe 
Powershell -command Remove-AppxPackage -Package Microsoft.ZuneVideo_2019.16112.11601.0_neutral_~_8wekyb3d8bbwe 
Powershell -command Remove-AppxProvisionedPackage -Online -PackageName Microsoft.ZuneVideo_2019.16112.11601.0_neutral_~_8wekyb3d8bbwe 

ECHO Packages non install�s par d�faut et t�l�charg�s automatiquement
:: version des packages HKEY_CLASSES_ROOT\ActivatableClasses\Package
Powershell -command Remove-AppxPackage -Package 4DF9E0F8.Netflix_6.15.59.0_x64__mcm4njqhnhss8 
Powershell -command Remove-AppxPackage -Package 4DF9E0F8.Netflix_6.17.61.0_x64__mcm4njqhnhss8 
Powershell -command Remove-AppxPackage -Package 4DF9E0F8.Netflix_6.18.89.0_x64__mcm4njqhnhss8
Powershell -command Remove-AppxPackage -Package Facebook.Facebook_71.684.7263.0_x86__8xx8rvfyw5nnt 
Powershell -command Remove-AppxPackage -Package Facebook.Facebook_75.700.56604.0_x86__8xx8rvfyw5nnt
Powershell -command Remove-AppxPackage -Package Facebook.Facebook_75.717.63029.0_x86__8xx8rvfyw5nnt 
Powershell -command Remove-AppxPackage -Package Facebook.Facebook_81.811.22862.0_x86__8xx8rvfyw5nnt
::Powershell -command Remove-AppxPackage -Package Microsoft.BingNews_4.17.74.0_x86__8wekyb3d8bbwe

:: test sylvain demontage appx
Powershell -command get-appxpackage *Microsoft.BingNews* | remove-appxpackage
Powershell -command get-appxpackage *89006A2E.AutodeskSketchBook* | remove-appxpackage
Powershell -command get-appxpackage *KeeperSecurityInc.Keeper* | remove-appxpackage


Powershell -command Remove-AppxPackage -Package king.com.ParadiseBay_2.1.0.0_x86__kgqvnymyfvs32 
Powershell -command Remove-AppxPackage -Package Microsoft.MinecraftUWP_0.1602.2.0_x64__8wekyb3d8bbwe
Powershell -command Remove-AppxPackage -Package Microsoft.MinecraftUWP_1.0.16.0_x64__8wekyb3d8bbwe
Powershell -command Remove-AppxPackage -Package Microsoft.MinecraftUWP_1.0.700.0_x64__8wekyb3d8bbwe
Powershell -command Remove-AppxPackage -Package Microsoft.MinecraftUWP_1.0.554.0_x64__8wekyb3d8bbwe 
Powershell -command Remove-AppxPackage -Package 9E2F88E3.Twitter_5.4.1.0_x86__wgeqdkkx372wm
Powershell -command Remove-AppxPackage -Package 9E2F88E3.Twitter_5.6.1.0_x86__wgeqdkkx372wm  
Powershell -command Remove-AppxPackage -Package A278AB0D.MarchofEmpires_2.3.1.1_x86__h6adky7gbf63m
Powershell -command Remove-AppxPackage -Package Microsoft.MicrosoftSudoku_1.4.1701.2602_x86__8wekyb3d8bbwe
Powershell -command Remove-AppxPackage -Package king.com.CandyCrushSodaSaga_1.81.900.0_x86__kgqvnymyfvs32 
Powershell -command Remove-AppxPackage -Package king.com.CandyCrushSodaSaga_1.86.700.0_x86__kgqvnymyfvs32
Powershell -command Remove-AppxPackage -Package flaregamesGmbH.RoyalRevolt2_2.7.0.2_x86__g0q0z3kw54rap
Powershell -command Remove-AppxPackage -Package Microsoft.Office.Sway_8wekyb3d8bbwe
Powershell -command Remove-AppxPackage -Package Microsoft.NetworkSpeedTest_8wekyb3d8bbwe
Powershell -command Remove-AppxPackage -Package Microsoft.MicrosoftPowerBIForWindows_8wekyb3d8bbwe
Powershell -command Remove-AppxPackage -Package Microsoft.BingNews_8wekyb3d8bbwe
Powershell -command Remove-AppxPackage -Package D5EA27B7.Duolingo-LearnLanguagesforFree_yx6k7tf7xvsea
Powershell -command Remove-AppxPackage -Package AdobeSystemsIncorporated.AdobePhotoshopExpress_ynb6jyjzte8ga
Powershell -command Remove-AppxPackage -Package ActiproSoftwareLLC.562882FEEB491_24pqs290vpjk0
Powershell -command Remove-AppxPackage -Package 46928bounde.EclipseManager_a5h4egax66k6y


ECHO Photos
Powershell -command Remove-AppxPackage -Package Microsoft.Windows.Photos_2016.511.9510.0_neutral_~_8wekyb3d8bbwe 
Powershell -command Remove-AppxProvisionedPackage -Online -PackageName Microsoft.Windows.Photos_2016.511.9510.0_neutral_~_8wekyb3d8bbwe 
:: Associer la visualisation des images � l�ancienne visionneuse de Windows photoviewer.dll
:: Sauvegarde des droits ACL des cl�s
Powershell -command "(get-acl -path "HKLM:\Software\Classes\CLSID\'{'FFE2A43C-56B9-4bf5-9A79-CC6D4285608A'}'").sddl" > SauveACL.txt
FOR /F %%i in ('type SauveACL.txt') do echo "machine\Software\Classes\CLSID\{FFE2A43C-56B9-4bf5-9A79-CC6D4285608A}",4,"%%i" > SauveACL.txt
Powershell -command "(get-acl -path "HKLM:\Software\Classes\PhotoViewer.FileAssoc.Tiff").sddl" > SauveACL2.txt
FOR /F %%j in ('type SauveACL2.txt') do echo "machine\Software\Classes\PhotoViewer.FileAssoc.Tiff",4,"%%j" > SauveACL2.txt

:: Modification des droits de la branche de registre (administrateur)
powershell -command (set-acl -on "HKLM\Software\Classes\CLSID\{FFE2A43C-56B9-4bf5-9A79-CC6D4285608A}" -ot reg -actn setowner -ownr "n:S-1-5-32-544;s:y" -rec yes >nul )
powershell -command (set-ACL -on "HKLM\Software\Classes\CLSID\{FFE2A43C-56B9-4bf5-9A79-CC6D4285608A}" -ot reg -actn ace -ace "n:S-1-5-32-544;s:y;p:full" -rec yes >nul )
powershell -command (set-ACL -on "HKLM\Software\Classes\PhotoViewer.FileAssoc.Tiff" -ot reg -actn setowner -ownr "n:S-1-5-32-544;s:y" -rec yes >nul ) 
powershell -command (set-ACL -on "HKLM\Software\Classes\PhotoViewer.FileAssoc.Tiff" -ot reg -actn ace -ace "n:S-1-5-32-544;s:y;p:full" -rec yes >nul )

Reg add "HKLM\Software\Classes\Applications\photoviewer.dll\shell\open" /v "MuiVerb" /t REG_SZ /d "@photoviewer.dll,-3043" /f >nul
Reg add "HKLM\Software\Classes\Applications\photoviewer.dll\shell\open\command" /ve /t REG_EXPAND_SZ /d "%%SystemRoot%%\System32\rundll32.exe \"%%ProgramFiles%%\Windows Photo Viewer\PhotoViewer.dll\", ImageView_Fullscreen %%1" /f >nul
Reg add "HKLM\Software\Classes\Applications\photoviewer.dll\shell\open\DropTarget" /v "Clsid" /t REG_SZ /d "{FFE2A43C-56B9-4bf5-9A79-CC6D4285608A}" /f >nul
Reg add "HKLM\Software\Classes\Applications\photoviewer.dll\shell\print\command" /ve /t REG_EXPAND_SZ /d "%%SystemRoot%%\System32\rundll32.exe \"%%ProgramFiles%%\Windows Photo Viewer\PhotoViewer.dll\", ImageView_Fullscreen %%1" /f >nul
Reg add "HKLM\Software\Classes\Applications\photoviewer.dll\shell\print\DropTarget" /v "Clsid" /t REG_SZ /d "{60fd46de-f830-4894-a628-6fa81bc0190d}" /f >nul
Reg add "HKLM\Software\Classes\PhotoViewer.FileAssoc.Gif" /v "FriendlyTypeName" /t REG_EXPAND_SZ /d "@%%ProgramFiles%%\Windows Photo Viewer\PhotoViewer.dll,-3057" /f >nul
Reg add "HKLM\Software\Classes\PhotoViewer.FileAssoc.Gif" /v "ImageOptionFlags" /t REG_DWORD /d "1" /f >nul
Reg add "HKLM\Software\Classes\PhotoViewer.FileAssoc.Gif\DefaultIcon" /ve /t REG_SZ /d "%%SystemRoot%%\System32\imageres.dll,-83" /f >nul
Reg add "HKLM\Software\Classes\PhotoViewer.FileAssoc.Gif\shell\open\command" /ve /t REG_EXPAND_SZ /d "%%SystemRoot%%\System32\rundll32.exe \"%%ProgramFiles%%\Windows Photo Viewer\PhotoViewer.dll\", ImageView_Fullscreen %%1" /f >nul
Reg add "HKLM\Software\Classes\PhotoViewer.FileAssoc.Gif\shell\open\DropTarget" /v "Clsid" /t REG_SZ /d "{FFE2A43C-56B9-4bf5-9A79-CC6D4285608A}" /f >nul
Reg add "HKLM\Software\Classes\PhotoViewer.FileAssoc.JFIF" /v "EditFlags" /t REG_DWORD /d "65536" /f >nul
Reg add "HKLM\Software\Classes\PhotoViewer.FileAssoc.JFIF" /v "FriendlyTypeName" /t REG_EXPAND_SZ /d "@%%ProgramFiles%%\Windows Photo Viewer\PhotoViewer.dll,-3055" /f >nul
Reg add "HKLM\Software\Classes\PhotoViewer.FileAssoc.JFIF" /v "ImageOptionFlags" /t REG_DWORD /d "1" /f >nul
Reg add "HKLM\Software\Classes\PhotoViewer.FileAssoc.JFIF\DefaultIcon" /ve /t REG_SZ /d "%%SystemRoot%%\System32\imageres.dll,-72" /f >nul
Reg add "HKLM\Software\Classes\PhotoViewer.FileAssoc.JFIF\shell\open" /v "MuiVerb" /t REG_EXPAND_SZ /d "@%%ProgramFiles%%\Windows Photo Viewer\photoviewer.dll,-3043" /f >nul
Reg add "HKLM\Software\Classes\PhotoViewer.FileAssoc.JFIF\shell\open\command" /ve /t REG_EXPAND_SZ /d "%%SystemRoot%%\System32\rundll32.exe \"%%ProgramFiles%%\Windows Photo Viewer\PhotoViewer.dll\", ImageView_Fullscreen %%1" /f >nul
Reg add "HKLM\Software\Classes\PhotoViewer.FileAssoc.JFIF\shell\open\DropTarget" /v "Clsid" /t REG_SZ /d "{FFE2A43C-56B9-4bf5-9A79-CC6D4285608A}" /f >nul
Reg add "HKLM\Software\Classes\PhotoViewer.FileAssoc.Jpeg" /v "EditFlags" /t REG_DWORD /d "65536" /f >nul
Reg add "HKLM\Software\Classes\PhotoViewer.FileAssoc.Jpeg" /v "FriendlyTypeName" /t REG_EXPAND_SZ /d "@%%ProgramFiles%%\Windows Photo Viewer\PhotoViewer.dll,-3055" /f >nul
Reg add "HKLM\Software\Classes\PhotoViewer.FileAssoc.Jpeg" /v "ImageOptionFlags" /t REG_DWORD /d "1" /f >nul
Reg add "HKLM\Software\Classes\PhotoViewer.FileAssoc.Jpeg\DefaultIcon" /ve /t REG_SZ /d "%%SystemRoot%%\System32\imageres.dll,-72" /f >nul
Reg add "HKLM\Software\Classes\PhotoViewer.FileAssoc.Jpeg\shell\open" /v "MuiVerb" /t REG_EXPAND_SZ /d "@%%ProgramFiles%%\Windows Photo Viewer\photoviewer.dll,-3043" /f >nul
Reg add "HKLM\Software\Classes\PhotoViewer.FileAssoc.Jpeg\shell\open\command" /ve /t REG_EXPAND_SZ /d "%%SystemRoot%%\System32\rundll32.exe \"%%ProgramFiles%%\Windows Photo Viewer\PhotoViewer.dll\", ImageView_Fullscreen %%1" /f >nul
Reg add "HKLM\Software\Classes\PhotoViewer.FileAssoc.Jpeg\shell\open\DropTarget" /v "Clsid" /t REG_SZ /d "{FFE2A43C-56B9-4bf5-9A79-CC6D4285608A}" /f >nul
Reg add "HKLM\Software\Classes\PhotoViewer.FileAssoc.Png" /v "FriendlyTypeName" /t REG_EXPAND_SZ /d "@%%ProgramFiles%%\Windows Photo Viewer\PhotoViewer.dll,-3057" /f >nul
Reg add "HKLM\Software\Classes\PhotoViewer.FileAssoc.Png" /v "ImageOptionFlags" /t REG_DWORD /d "1" /f >nul
Reg add "HKLM\Software\Classes\PhotoViewer.FileAssoc.Png\DefaultIcon" /ve /t REG_SZ /d "%%SystemRoot%%\System32\imageres.dll,-71" /f >nul
Reg add "HKLM\Software\Classes\PhotoViewer.FileAssoc.Png\shell\open\command" /ve /t REG_EXPAND_SZ /d "%%SystemRoot%%\System32\rundll32.exe \"%%ProgramFiles%%\Windows Photo Viewer\PhotoViewer.dll\", ImageView_Fullscreen %%1" /f >nul
Reg add "HKLM\Software\Classes\PhotoViewer.FileAssoc.Png\shell\open\DropTarget" /v "Clsid" /t REG_SZ /d "{FFE2A43C-56B9-4bf5-9A79-CC6D4285608A}" /f >nul
Reg add "HKLM\Software\Classes\PhotoViewer.FileAssoc.Tiff" /v "FriendlyTypeName" /t REG_EXPAND_SZ /d "@%%ProgramFiles%%\Windows Photo Viewer\PhotoViewer.dll,-3058" /f >nul
Reg add "HKLM\Software\Classes\PhotoViewer.FileAssoc.Tiff" /v "ImageOptionFlags" /t REG_DWORD /d "1" /f >nul
Reg add "HKLM\Software\Classes\PhotoViewer.FileAssoc.Tiff\DefaultIcon" /ve /t REG_SZ /d "%%SystemRoot%%\System32\imageres.dll,-122" /f >nul
Reg add "HKLM\Software\Classes\PhotoViewer.FileAssoc.Tiff\shell\open" /v "MuiVerb" /t REG_EXPAND_SZ /d "@%%ProgramFiles%%\Windows Photo Viewer\photoviewer.dll,-3043" /f >nul
Reg add "HKLM\Software\Classes\PhotoViewer.FileAssoc.Tiff\shell\open\command" /ve /t REG_EXPAND_SZ /d "%%SystemRoot%%\System32\rundll32.exe \"%%ProgramFiles%%\Windows Photo Viewer\PhotoViewer.dll\", ImageView_Fullscreen %%1" /f >nul
Reg add "HKLM\Software\Classes\PhotoViewer.FileAssoc.Tiff\shell\open\DropTarget" /v "Clsid" /t REG_SZ /d "{FFE2A43C-56B9-4bf5-9A79-CC6D4285608A}" /f >nul
Reg add "HKLM\Software\Classes\PhotoViewer.FileAssoc.Wdp" /v "EditFlags" /t REG_DWORD /d "65536" /f >nul
Reg add "HKLM\Software\Classes\PhotoViewer.FileAssoc.Wdp" /v "ImageOptionFlags" /t REG_DWORD /d "1" /f >nul
Reg add "HKLM\Software\Classes\PhotoViewer.FileAssoc.Wdp\DefaultIcon" /ve /t REG_SZ /d "%%SystemRoot%%\System32\wmphoto.dll,-400" /f >nul
Reg add "HKLM\Software\Classes\PhotoViewer.FileAssoc.Wdp\shell\open" /v "MuiVerb" /t REG_EXPAND_SZ /d "@%%ProgramFiles%%\Windows Photo Viewer\photoviewer.dll,-3043" /f >nul
Reg add "HKLM\Software\Classes\PhotoViewer.FileAssoc.Wdp\shell\open\command" /ve /t REG_EXPAND_SZ /d "%%SystemRoot%%\System32\rundll32.exe \"%%ProgramFiles%%\Windows Photo Viewer\PhotoViewer.dll\", ImageView_Fullscreen %%1" /f >nul
Reg add "HKLM\Software\Classes\PhotoViewer.FileAssoc.Wdp\shell\open\DropTarget" /v "Clsid" /t REG_SZ /d "{FFE2A43C-56B9-4bf5-9A79-CC6D4285608A}" /f >nul
Reg add "HKLM\Software\Classes\PhotoViewer.FileAssoc.Bitmap" /v "FriendlyTypeName" /t REG_EXPAND_SZ /d "@%%ProgramFiles%%\Windows Photo Viewer\PhotoViewer.dll,-3056" /f >nul
Reg add "HKLM\Software\Classes\PhotoViewer.FileAssoc.Bitmap" /v "ImageOptionFlags" /t REG_DWORD /d "1" /f >nul
Reg add "HKLM\Software\Classes\PhotoViewer.FileAssoc.Bitmap\DefaultIcon" /ve /t REG_SZ /d "%%SystemRoot%%\System32\imageres.dll,-70" /f >nul
Reg add "HKLM\Software\Classes\PhotoViewer.FileAssoc.Bitmap\shell\open\command" /ve /t REG_EXPAND_SZ /d "%%SystemRoot%%\System32\rundll32.exe \"%%ProgramFiles%%\Windows Photo Viewer\PhotoViewer.dll\", ImageView_Fullscreen %%1" /f >nul
Reg add "HKLM\Software\Classes\PhotoViewer.FileAssoc.Bitmap\shell\open\DropTarget" /v "Clsid" /t REG_SZ /d "{FFE2A43C-56B9-4bf5-9A79-CC6D4285608A}" /f >nul
Reg add "HKLM\Software\Microsoft\Windows Photo Viewer\Capabilities" /v "ApplicationDescription" /t REG_SZ /d "@%%ProgramFiles%%\Windows Photo Viewer\photoviewer.dll,-3069" /f >nul
Reg add "HKLM\Software\Microsoft\Windows Photo Viewer\Capabilities" /v "ApplicationName" /t REG_SZ /d "@%%ProgramFiles%%\Windows Photo Viewer\photoviewer.dll,-3009" /f >nul
Reg add "HKLM\Software\Microsoft\Windows Photo Viewer\Capabilities\FileAssociations" /v ".bmp" /t REG_SZ /d "PhotoViewer.FileAssoc.Bitmap" /f >nul
Reg add "HKLM\Software\Microsoft\Windows Photo Viewer\Capabilities\FileAssociations" /v ".dib" /t REG_SZ /d "PhotoViewer.FileAssoc.Bitmap" /f >nul
Reg add "HKLM\Software\Microsoft\Windows Photo Viewer\Capabilities\FileAssociations" /v ".gif" /t REG_SZ /d "PhotoViewer.FileAssoc.Gif" /f >nul
Reg add "HKLM\Software\Microsoft\Windows Photo Viewer\Capabilities\FileAssociations" /v ".jfif" /t REG_SZ /d "PhotoViewer.FileAssoc.JFIF" /f >nul
Reg add "HKLM\Software\Microsoft\Windows Photo Viewer\Capabilities\FileAssociations" /v ".jpe" /t REG_SZ /d "PhotoViewer.FileAssoc.Jpeg" /f >nul
Reg add "HKLM\Software\Microsoft\Windows Photo Viewer\Capabilities\FileAssociations" /v ".jpeg" /t REG_SZ /d "PhotoViewer.FileAssoc.Jpeg" /f >nul
Reg add "HKLM\Software\Microsoft\Windows Photo Viewer\Capabilities\FileAssociations" /v ".jpg" /t REG_SZ /d "PhotoViewer.FileAssoc.Jpeg" /f >nul
Reg add "HKLM\Software\Microsoft\Windows Photo Viewer\Capabilities\FileAssociations" /v ".jxr" /t REG_SZ /d "PhotoViewer.FileAssoc.Wdp" /f >nul
Reg add "HKLM\Software\Microsoft\Windows Photo Viewer\Capabilities\FileAssociations" /v ".png" /t REG_SZ /d "PhotoViewer.FileAssoc.Png" /f >nul
Reg add "HKLM\Software\Microsoft\Windows Photo Viewer\Capabilities\FileAssociations" /v ".tif" /t REG_SZ /d "PhotoViewer.FileAssoc.Tiff" /f >nul
Reg add "HKLM\Software\Microsoft\Windows Photo Viewer\Capabilities\FileAssociations" /v ".tiff" /t REG_SZ /d "PhotoViewer.FileAssoc.Tiff" /f >nul
Reg add "HKLM\Software\Microsoft\Windows Photo Viewer\Capabilities\FileAssociations" /v ".wdp" /t REG_SZ /d "PhotoViewer.FileAssoc.Wdp" /f >nul

:: Restauration des droits ACL
powershell -command (SET-ACL -on "HKLM\Software\Classes\CLSID\{FFE2A43C-56B9-4bf5-9A79-CC6D4285608A}" -ot reg -actn restore -bckp SauveACL.txt )
powershell -command (SET-ACL -on "HKLM\Software\Classes\PhotoViewer.FileAssoc.Tiff" -ot reg -actn restore -bckp SauveACL2.txt )
DEL SauveACL*.txt


REM Suppression de Contacter le support, Assistance rapide et MediaPlayer
REM afficher la liste des applications pouvant �tre supprim� par DISM /Online /Get-Capabilities /limitaccess
REM DISM /Online /Remove-Capability /CapabilityName:App.Support.ContactSupport~~~~0.0.1.0 /Limitacess
REM DISM /Online /Remove-Capability /CapabilityName:App.Support.Quickassist~~~~0.0.1.0 /Limitaccess
REM DISM /Online /Remove-Capability /CapabilityName:Media.WindowsMediaPlayer~~~~0.0.12.0 /Limitaccess

rem ou avec powershell mais une connexion internet est necessaire
rem Powershell -command Get-WindowsCapability -online | ? {$_.Name -like '*ContactSupport*'} | Remove-WindowsCapability �online
rem Powershell -command Get-WindowsCapability -online | ? {$_.Name -like '*Quickassist*'} | Remove-WindowsCapability �online


::===========================================================================================================
ECHO. Suppression de Onedrive
::===========================================================================================================
set x86="%SYSTEMROOT%\System32\OneDriveSetup.exe"
set x64="%SYSTEMROOT%\SysWOW64\OneDriveSetup.exe"

echo Fermeture du process OneDrive.
echo.
TASKKILL /F /IM OneDrive.exe /T > NUL 2>&1
PING 127.0.0.1 -n 5 > NUL 2>&1

echo D�sinstallation de OneDrive.
echo.
if exist %x64% (
%x64% /uninstall
) else (
%x86% /uninstall
)
PING 127.0.0.1 -n 5 > NUL 2>&1

echo Suppression des restes de onedrive.
echo.
rd /s /q "%UserProfile%\OneDrive" 
rd /s /q "%SystemDrive%\OneDriveTemp" 
rd /s /q "%LocalAppData%\Microsoft\OneDrive"
rd /s /q "%ProgramData%\Microsoft OneDrive" 

echo Retrait onedrive du Panneau lat�ral de l'explorateur.
echo.
REG DELETE "HKCR\CLSID\{018D5C66-4533-4307-9B53-224DE2ED1FE6}" /REG:32 /f 
REG DELETE "HKCR\CLSID\{018D5C66-4533-4307-9B53-224DE2ED1FE6}" /REG:64 /f 

REG DELETE "HKCU\Software\Microsoft\Windows\CurrentVersion\Run" /v "OneDrive" /f 

:: suppression du lancement de onedrive lors de la cr�ation d'un nouveau compte
reg load "HKU\Default" "C:\Users\Default\NTUSER.DAT" 
reg delete "HKU\Default\Software\Microsoft\Windows\CurrentVersion\Run" /v OneDriveSetup /f 
reg unload "HKU\Default" 


pause