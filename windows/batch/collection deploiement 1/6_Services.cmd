@ECHO OFF

::===========================================================================================================
ECHO. D�sactivation des services de t�l�m�trie
::===========================================================================================================
:: Supression des 4 services de t�l�m�trie les plus indiscrets 
set srv_espion=^
	DiagTrack,dmwappushservice,diagnosticshub.standardcollector.service,^
	RetailDemo
for %%i in (%srv_espion%) do (
	sc query %%i > nul
	if not errorlevel 1060 (
		echo Service : %%i
		sc stop %%i > nul
		sc delete %%i
	)
)

:: D�sactivation des services
set srv_espion2=^
	WerSvc,PcaSvc,DoSvc,WMPNetworkSvc,XblAuthManager,XblGameSave,XboxNetApiSvc,^
	xboxgip,lfsvc,NcbService,WbioSrvc,LicenseManager,OneSyncSvc,^
	CDPUserSvc,MapsBroker,PhoneSvc,WalletService,SensorDataService,^
	SensorService,SensrSvc,RemoteRegistry,TermService
for %%i in (%srv_espion%) do (
	echo Service : %%i
	sc stop %%i > nul
	sc config %%i start= disabled
)

:: D�sactivation des services ne pouvant pas �tre arr�t�s par la commande SC et contenant des caract�res al�atoires
FOR /F "tokens=5 delims=\" %%G in ('reg query "HKLM\SYSTEM\ControlSet001\Services" /f "DevicesFlowUserSvc_"') do (
   FOR /F "tokens=2 delims=_" %%a in ("%%G") do set xxxxx=%%a
)

set srv_espion3=^
	MessagingService,MessagingService_%xxxxx%,PimIndexMaintenanceSvc,^
	PimIndexMaintenanceSvc_%xxxxx%,UnistoreSvc,UnistoreSvc_%xxxxx%,UserDataSvc,^
	UserDataSvc_%xxxxx%,OneSyncSvc_%xxxxx%,CDPUserSvc_%xxxxx%
for %%i in (%srv_espion3%) do (
	echo Service : %%i
	sc stop %%i > nul
	reg add "HKLM\SYSTEM\ControlSet001\Services\%%i" /v "Start" /t REG_DWORD /d 4 /f	
)

::Powershell -command (get-acl -path "hklm:\SYSTEM\ControlSet001\Services\DPS").sddl
::Powershell -command "get-acl -path "hklm:\SYSTEM\ControlSet001\Services\DPS" | Format-List"
echo "machine\SYSTEM\ControlSet001\Services\DPS",4,"O:BAG:BAD:PAI(A;;KA;;;SY)(A;CIIO;GA;;;SY)(A;;KR;;;BA)(A;CIIO;GXGR;;;BA)(A;;KR;;;BU)(A;CIIO;GXGR;;;BU)" > SauveACL.txt 
Powershell -command(setACL -on "HKLM\SYSTEM\ControlSet001\Services\DPS" -ot reg -actn setowner -ownr "n:S-1-5-32-544;s:y" -rec yes >nul 2>&1 )
Powershell -command(setACL -on "HKLM\SYSTEM\ControlSet001\Services\DPS" -ot reg -actn ace -ace "n:S-1-5-32-544;s:y;p:full" -rec yes >nul 2>&1 )
REG ADD "HKLM\SYSTEM\ControlSet001\Services\DPS" /v start /t REG_DWORD /d 4 /f
Powershell -command(SetACL -on "HKLM\SYSTEM\ControlSet001\Services\DPS" -ot reg -actn restore -bckp SauveACL.txt >nul 2>&1)
DEL SauveACL.txt >nul 2>&1

::Powershell -command "get-acl -path "hklm:\SYSTEM\ControlSet001\Services\DPS" | Format-List"
echo "machine\SYSTEM\ControlSet001\Services\TrkWks",4,"O:BAG:BAD:PAI(A;;KA;;;SY)(A;CIIO;GA;;;SY)(A;;KR;;;BA)(A;CIIO;GXGR;;;BA)(A;;KR;;;BU)(A;CIIO;GXGR;;;BU)" > SauveACL.txt 
setACL -on "HKLM\SYSTEM\ControlSet001\Services\TrkWks" -ot reg -actn setowner -ownr "n:S-1-5-32-544;s:y" -rec yes >nul 2>&1 
setACL -on "HKLM\SYSTEM\ControlSet001\Services\TrkWks" -ot reg -actn ace -ace "n:S-1-5-32-544;s:y;p:full" -rec yes >nul 2>&1 
REG ADD "HKLM\SYSTEM\ControlSet001\Services\TrkWks" /v start /t REG_DWORD /d 4 /f
SetACL -on "HKLM\SYSTEM\ControlSet001\Services\TrkWks" -ot reg -actn restore -bckp SauveACL.txt >nul 2>&1
DEL SauveACL.txt >nul 2>&1

:: D�sactivation des services de logs
REG ADD "HKEY_LOCAL_MACHINE\SYSTEM\ControlSet001\Control\WMI\AutoLogger\AutoLogger-Diagtrack-Listener" /v "Start" /t REG_DWORD /d 0 /f 
REG ADD "HKEY_LOCAL_MACHINE\SYSTEM\ControlSet001\Control\WMI\AutoLogger\SQMLogger" /v "Start" /t REG_DWORD /d 0 /f 

SET xxxxx=

:: desactivation de windows defender ( necessaire pour permettre  l'utilisation de Mcafee)
REG ADD "HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows Defender\" /v "DisableAntispyware" /t REG_DWORD /d 1 /f


::===========================================================================================================
:: echo Suppression des services spyware/bloatware...
::===========================================================================================================
::set spy_services=^
::	DiagTrack,dmwappushservice,diagnosticshub.standardcollector.service,DcpSvc,^
::	WerSvc,PcaSvc,DoSvc,WMPNetworkSvc,XblAuthManager,XblGameSave,XboxNetApiSvc,^
::	xboxgip,wlidsvc,lfsvc,NcbService,WbioSrvc,LicenseManager,OneSyncSvc,CDPSvc,^
::	CDPUserSvc,MapsBroker,PhoneSvc,RetailDemo,WalletService
::for %%i in (%spy_services%) do (
::	sc query %%i > nul
::	if not errorlevel 1060 (
::		echo Service : %%i
::		sc stop %%i > nul
::		sc delete %%i
::	)
::)
